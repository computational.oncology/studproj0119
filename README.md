# StudProj0119
A group folder for student projects starting Jan 2019

Please ensure that there are two subfolders, and keep relevant code in each

SurvProj: Starting with Titanic data and working up to looking at more complex Survival Data, eventually using Simulacrum

GanProj: Using GANs to generate synthetic data; starting with Titantic data, and working up to replicating RTDS

