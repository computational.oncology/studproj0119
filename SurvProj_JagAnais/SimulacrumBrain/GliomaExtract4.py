#A basic Simulacrum script to demonstrate some usage
#Matt Williams 2018
#matthew.williams@imperial.ac.uk

import pandas as pd
import datetime
from lifelines import KaplanMeierFitter
from lifelines.datasets import load_dd
import numpy as np
import time
data = load_dd()
#print(data.sample(6))

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_rows', 1000000)

pat_file = "sim_av_patient.csv"
tum_file = "sim_av_tumour.csv"
regimen_file = "sim_sact_regimen.csv"
sactPatient_file = "sim_sact_patient.csv"
sactTumour_file = "sim_sact_tumour.csv"
patients = pd.read_csv(pat_file)
tumours = pd.read_csv(tum_file, low_memory = False)
regimen = pd.read_csv(regimen_file, low_memory = False, encoding = "ISO-8859-1")
sactPat = pd.read_csv(sactPatient_file, low_memory = False, encoding = "ISO-8859-1")
sactTum = pd.read_csv(sactTumour_file, low_memory = False, encoding = "ISO-8859-1")

drug = pd.read_csv("sim_sact_drug_detail.csv")
outcome = pd.read_csv("sim_sact_outcome.csv")

print("*******************")

glioma = tumours[tumours['SITE_ICD10_O2_3CHAR'] == 'C71']
#print(glioma.count())
grp_behaviour = glioma.groupby(['MORPH_ICD10_O2','BEHAVIOUR_ICD10_O2'])
#print(grp_behaviour['TUMOURID'].count())

glioma_malignant = glioma[glioma['BEHAVIOUR_ICD10_O2'] == '3']
malig_grp = glioma_malignant.groupby(['MORPH_ICD10_O2'])
#print(malig_grp['TUMOURID'].count())

#print(patients.head(3))
#print(glioma_malignant.head(3))
#Now we merge data on glioma cancers and patients
#And then drop breast and prostate cancer scores
glioma_cancer_patients = pd.merge(patients, glioma_malignant, on = 'PATIENTID')
glioma_CP_reduced = glioma_cancer_patients.drop(["GLEASON_TERTIARY", "GLEASON_COMBINED", "ER_SCORE",   "PR_SCORE", "ER_STATUS", "PR_STATUS", "GLEASON_PRIMARY",  "GLEASON_SECONDARY", "HER2_STATUS", "SCREENINGSTATUSFULL_CODE", "T_BEST", "N_BEST", "M_BEST"], axis = 1)

#print("\n GLIOMA CP \n", glioma_CP_reduced.columns)
#print("\n REG: \n", regimen.columns)
#print(regimen.head(3))
print("\n Patient: \n", patients.columns)
print("\n SACTPAT: \n", sactPat.columns)
print("\n Tumour: \n", tumours.columns)
print("\n SACTTUM: \n", sactTum.columns)
print("\n SACTReg: \n", regimen.columns)
#print("\n SACTReg: \n", regimen.groupby(["MAPPED_REGIMEN"]).sum()) #Too long to be usefuls

print("\n Glioma_CP_Reduced: \n", glioma_CP_reduced.columns)
#Now we link our glioma patients with SACTPat
glioma_SACTLink = pd.merge(glioma_CP_reduced, sactPat, how="left", left_on =['LINKNUMBER_x'], right_on=['LINK_NUMBER'])
print("\n Glioma_SACTPAT: \n", glioma_SACTLink.columns)

#Now merge SACTTum and SACTReg
SACTTum_Reg = pd.merge(sactTum, regimen, how = "left", on="MERGED_TUMOUR_ID")
#print("\n SACTTum_Reg: \n", SACTTum_Reg.columns)
#print("\n SACTTum_Reg: \n", SACTTum_Reg.head(3))
#print("\n SACTTum_Reg: \n", SACTTum_Reg.groupby(["PRIMARY_DIAGNOSIS"]).sum())

#Brain codes
BrainC = ("c713", "C710", "C711", "C712", "C713", "C714", "C715", "C716", "C717", "C718", "C719")
SACTTum_Reg_Brain = SACTTum_Reg[SACTTum_Reg['PRIMARY_DIAGNOSIS'].isin(BrainC)]
SACTTum_Reg_Brain_TEMO = SACTTum_Reg_Brain[SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("TEMOZ") == True] #Need to handle y and Y and 2

# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.columns)
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.head(3))
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.groupby(["PRIMARY_DIAGNOSIS"]).sum())
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.groupby(["MAPPED_REGIMEN"]).sum())
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.groupby(["CHEMO_RADIATION"]).sum())

print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.columns)
print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.head(3))
# print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.groupby(["PRIMARY_DIAGNOSIS"]).sum())
# print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.groupby(["MAPPED_REGIMEN"]).sum())



# print("\n Glioma_SACTLink: \n", glioma_SACTLink.columns)
# print("\n Glioma_SACTLink: \n", glioma_SACTLink.head(3))
#
# #Now we merge the glioma_SACTlink with SACT_Reg_Brain_TEMO
glioma_CP_Red_TEMOReg = pd.merge(glioma_SACTLink, SACTTum_Reg_Brain_TEMO, how="inner", left_on =['PATIENTID'], right_on=['MERGED_PATIENT_ID_x'])
#
print("\n Glioma CP Red_TEMOReg: \n", glioma_CP_Red_TEMOReg.columns)
print(glioma_CP_Red_TEMOReg.head(3))

outcome_drug = pd.merge(outcome, drug,  how = 'inner', on = 'MERGED_PATIENT_ID')
glioma_FINAL = pd.merge(glioma_CP_Red_TEMOReg, outcome_drug, how = 'left', on = 'MERGED_PATIENT_ID')


#Adding Jag & Anais' work here
#CLEAN SOME STUFF
glioma_FINAL.drop(['SEX_x', 'LINKNUMBER_x', 'LINKNUMBER_y', 'MERGED_REGIMEN_ID_x','MERGED_TUMOUR_ID_x', 'MERGED_TUMOUR_ID_y','MERGED_REGIMEN_ID_y', 'MERGED_PATIENT_ID_x', 'MERGED_PATIENT_ID_y'], axis = 1, inplace = True)
#print(glioma_FINAL.columns)

print('Checking the number of missing values before')
print('MISSING DATA:', glioma_FINAL.isnull().sum())

#Based on the missing data we decided to drop the following
glioma_FINAL.drop(['DEATHCAUSECODE_1B', 'DEATHCAUSECODE_1C', 'STAGE_BEST_SYSTEM', 'REGIMEN_OUTCOME_SUMMARY'], axis = 1, inplace = True)
#print(glioma_FINAL.columns)


#CREATING A NEW VARIABLE --> HAD_SURGERY
glioma_FINAL['DIAGNOSISDATEBEST'] = pd.to_datetime(glioma_FINAL['DIAGNOSISDATEBEST'])
glioma_FINAL['DATE_FIRST_SURGERY'] = pd.to_datetime(glioma_FINAL['DATE_FIRST_SURGERY'])
glioma_FINAL['HAD_SURGERY'] = (glioma_FINAL['DIAGNOSISDATEBEST'] - glioma_FINAL['DATE_FIRST_SURGERY']) > np.timedelta64(3, "M")

glioma_FINAL['ETHNICITY'] = glioma_FINAL['ETHNICITY'].fillna('Z')

#From Sally Vernon sally.vernon@phe.gov.uk document - Brain_Grouping
#C729 means unknwown location in the CNS
glioma_FINAL['SITE_ICD10_O2'] = glioma_FINAL['SITE_ICD10_O2'].fillna('C729')

#Anais cheat sheet
#GX means grade of differentiation is not appropriate or cannot be assessed
glioma_FINAL['GRADE'] = glioma_FINAL['GRADE'].fillna('GX')

#https://www.datadictionary.nhs.uk/data_dictionary/data_field_notes/c/cancer_care_plan_intent_de.asp?shownav=1
# 9 = not known
glioma_FINAL['CANCERCAREPLANINTENT'] = glioma_FINAL['CANCERCAREPLANINTENT'].fillna('9')

glioma_FINAL['CNS'] = glioma_FINAL['CNS'].fillna('99')
#glioma_FINAL['CNS'] = glioma_FINAL['CNS'].replace('Y1m', 'Y1')

glioma_FINAL['ACE27'] = glioma_FINAL['ACE27'].fillna('9')

#over16 = glioma_FINAL.loc[glioma_FINAL['AGE'] > 16]
glioma_FINAL['PERFORMANCESTATUS'] = glioma_FINAL['PERFORMANCESTATUS'].fillna('9')
#glioma_FINAL['PERFORMANCESTATUS'] = glioma_FINAL['PERFORMANCESTATUS'].replace('1m', 1)
#glioma_FINAL['PERFORMANCESTATUS'] = glioma_FINAL['PERFORMANCESTATUS'].astype(str)

#QUINTILE_2015
glioma_FINAL['QUINTILE_2015'] = glioma_FINAL['QUINTILE_2015'].replace('5 - most deprived', 5)
glioma_FINAL['QUINTILE_2015'] = glioma_FINAL['QUINTILE_2015'].replace('1 - least deprived', 1)
glioma_FINAL['QUINTILE_2015'] = glioma_FINAL['QUINTILE_2015'].astype(int)

#ADDING DATES TO DATE_FIRST_SURGERY
diff = glioma_FINAL['DATE_FIRST_SURGERY'] - glioma_FINAL['DIAGNOSISDATEBEST']
#print("diff1", diff.mean())
#print("diff1", diff.unique())
glioma_FINAL['DATE_FIRST_SURGERY'] = glioma_FINAL['DATE_FIRST_SURGERY'].fillna(glioma_FINAL['DIAGNOSISDATEBEST'] + datetime.timedelta(days = 3, hours = 10.25))

#HEIGHT_AT_START_OF_REGIMEN
#right now, only replacing missing heights with mean
height_mean = glioma_FINAL['HEIGHT_AT_START_OF_REGIMEN'].mean()
glioma_FINAL['HEIGHT_AT_START_OF_REGIMEN'] = glioma_FINAL['HEIGHT_AT_START_OF_REGIMEN'].fillna(height_mean)
#print("STANDARD DEVIATION HEIGHT", np.std(glioma_FINAL['HEIGHT_AT_START_OF_REGIMEN']))

#Below: heights by age / sex --> values are very close

#adult_female = glioma_FINAL.loc[(glioma_FINAL['AGE'] > 16) & (glioma_FINAL['SEX_y'] == 2)]
#height_meanF = adult_female['HEIGHT_AT_START_OF_REGIMEN'].mean()
#print("ADULT FEMALE", height_meanF)

#adult_male = glioma_FINAL.loc[(glioma_FINAL['AGE'] > 16) & (glioma_FINAL['SEX_y'] == 1)]
#height_meanM = adult_male['HEIGHT_AT_START_OF_REGIMEN'].mean()
#print("ADULT MALE", height_meanM)

#children_female = glioma_FINAL.loc[(glioma_FINAL['AGE'] <= 16) & (glioma_FINAL['SEX_y'] == 2)]
#height_meanCF = children_female['HEIGHT_AT_START_OF_REGIMEN'].mean()
#print("CHILDREN FEMALE", height_meanCF)

#children_male = glioma_FINAL.loc[(glioma_FINAL['AGE'] <= 16) & (glioma_FINAL['SEX_y'] == 1)]
#height_meanCM = children_male['HEIGHT_AT_START_OF_REGIMEN'].mean()
#print("MALE CHILDREN", height_meanCM)

#WEIGHT_AT_START_OF_REGIMEN
#right now, only replacing missing weights with mean
#will find means by age / sex later
#standard is high --> will change later
weight_mean = glioma_FINAL['WEIGHT_AT_START_OF_REGIMEN'].mean()
glioma_FINAL['WEIGHT_AT_START_OF_REGIMEN'] = glioma_FINAL['WEIGHT_AT_START_OF_REGIMEN'].fillna(weight_mean)
#print("STANDARD DEVIATION WEIGHT", np.std(glioma_FINAL['WEIGHT_AT_START_OF_REGIMEN']))

#drug treatment intent
glioma_FINAL['INTENT_OF_TREATMENT'] = glioma_FINAL['INTENT_OF_TREATMENT'].fillna("9")
glioma_FINAL['INTENT_OF_TREATMENT'] = glioma_FINAL['INTENT_OF_TREATMENT'].replace(4, "9")

glioma_FINAL['CLINICAL_TRIAL'] = glioma_FINAL['CLINICAL_TRIAL'].fillna("99")

glioma_FINAL['ADMINISTRATION_ROUTE'] = glioma_FINAL['ADMINISTRATION_ROUTE'].fillna("99")

#ADDING DATES TO DATE_DECISION_TO_TREAT
glioma_FINAL['DATE_DECISION_TO_TREAT'] = pd.to_datetime(glioma_FINAL['DATE_DECISION_TO_TREAT'])
glioma_FINAL['START_DATE_OF_REGIMEN'] = pd.to_datetime(glioma_FINAL['START_DATE_OF_REGIMEN'])
diff2 = glioma_FINAL['START_DATE_OF_REGIMEN'] - glioma_FINAL['DATE_DECISION_TO_TREAT']
#print("diff2", diff2.mean())
#print("diff2", diff2.unique())
glioma_FINAL['DATE_DECISION_TO_TREAT'] = glioma_FINAL['DATE_DECISION_TO_TREAT'].fillna(glioma_FINAL['START_DATE_OF_REGIMEN'] - datetime.timedelta(days = 21, hours = 8.42))

#ADDING DATES TO DATE_OF_FINAL_TREATMENT
glioma_FINAL['DATE_OF_FINAL_TREATMENT'] = pd.to_datetime(glioma_FINAL['DATE_OF_FINAL_TREATMENT'])
diff3 = glioma_FINAL['DATE_OF_FINAL_TREATMENT'] - glioma_FINAL['START_DATE_OF_REGIMEN']
#print("diff3", diff3.mean())
#print("diff3", diff3.unique())
glioma_FINAL['DATE_OF_FINAL_TREATMENT'] = glioma_FINAL['DATE_OF_FINAL_TREATMENT'].fillna(glioma_FINAL['START_DATE_OF_REGIMEN'] + datetime.timedelta(days = 88))

#dropping CHEMO_RADIATION because contradicts DRUG_GROUP
glioma_FINAL.drop(['CHEMO_RADIATION'], axis = 1, inplace = True)

#ADMINISTRATION_DATE
#relevant?
glioma_FINAL['ADMINISTRATION_DATE'] = pd.to_datetime(glioma_FINAL['ADMINISTRATION_DATE'])
diff4 = glioma_FINAL['ADMINISTRATION_DATE'] - glioma_FINAL['START_DATE_OF_REGIMEN']
#print("diff4", diff4.mean())
#print("diff4", diff4.unique())
glioma_FINAL['ADMINISTRATION_DATE'] = glioma_FINAL['ADMINISTRATION_DATE'].fillna(glioma_FINAL['START_DATE_OF_REGIMEN'] + datetime.timedelta(days = 81, hours = 4.7))


#FILLING IN WHAT WE DON'T KNOW WITH "9"
glioma_FINAL['REGIMEN_MOD_DOSE_REDUCTION'] = glioma_FINAL['REGIMEN_MOD_DOSE_REDUCTION'].fillna("9")
glioma_FINAL['REGIMEN_MOD_STOPPED_EARLY'] = glioma_FINAL['REGIMEN_MOD_STOPPED_EARLY'].fillna("9")
glioma_FINAL['REGIMEN_MOD_TIME_DELAY'] = glioma_FINAL['REGIMEN_MOD_TIME_DELAY'].fillna("9")
glioma_FINAL['OPCS_CODE_OF_PROVIDER'] = glioma_FINAL['REGIMEN_MOD_TIME_DELAY'].fillna("9")
glioma_FINAL['OPCS_DELIVERY_CODE'] = glioma_FINAL['OPCS_DELIVERY_CODE'].fillna("9")
glioma_FINAL['MORPHOLOGY_CLEAN'] = glioma_FINAL['MORPHOLOGY_CLEAN'].fillna("9")
glioma_FINAL['DEATHLOCATIONCODE'] = glioma_FINAL['DEATHLOCATIONCODE'].fillna("9")
glioma_FINAL['DEATHCAUSECODE_UNDERLYING'] = glioma_FINAL['DEATHCAUSECODE_UNDERLYING'].fillna("9")
glioma_FINAL['DEATHCAUSECODE_2'] = glioma_FINAL['DEATHCAUSECODE_2'].fillna("9")
glioma_FINAL['DEATHCAUSECODE_1A'] = glioma_FINAL['DEATHCAUSECODE_1A'].fillna("9")

glioma_FINAL['ACTUAL_DOSE_PER_ADMINISTRATION'] = glioma_FINAL['ACTUAL_DOSE_PER_ADMINISTRATION'].fillna("XXX")

print('MISSING DATA:', glioma_FINAL.isnull().sum())

df = glioma_FINAL.applymap(str)

#for value in glioma_FINAL['PATIENTID']:
#    print(type(value))
#    break

#ONE HOT ENCODING
one_hot1 = pd.get_dummies(glioma_FINAL['ACE27'])
df = glioma_FINAL.drop('ACE27', axis = 1)
df = glioma_FINAL.join(one_hot1, lsuffix = '_ACE27')

one_hot2 = pd.get_dummies(glioma_FINAL['BEHAVIOUR_ICD10_O2'])
df = glioma_FINAL.drop('BEHAVIOUR_ICD10_O2', axis = 1)
df = glioma_FINAL.join(one_hot2, lsuffix = '_BEHAVIOUR_ICD10_O2')

one_hot3 = pd.get_dummies(glioma_FINAL['CANCERCAREPLANINTENT'])
df = glioma_FINAL.drop('CANCERCAREPLANINTENT', axis = 1)
df = glioma_FINAL.join(one_hot3, lsuffix = '_CANCERCAREPLANINTENT')

one_hot4 = pd.get_dummies(glioma_FINAL['CNS'])
df = glioma_FINAL.drop('CNS', axis = 1)
df = glioma_FINAL.join(one_hot4, lsuffix = '_CANCERCAREPLANINTENT')

one_hot5 = pd.get_dummies(glioma_FINAL['CREG_CODE'])
df = glioma_FINAL.drop('CREG_CODE', axis = 1)
df = glioma_FINAL.join(one_hot5, lsuffix = '_CREG_CODE')

one_hot7 = pd.get_dummies(glioma_FINAL['GRADE'])
df = glioma_FINAL.drop('GRADE', axis = 1)
df = glioma_FINAL.join(one_hot7, lsuffix = '_GRADE')

one_hot8 = pd.get_dummies(glioma_FINAL['LATERALITY'])
df = glioma_FINAL.drop('LATERALITY', axis = 1)
df = glioma_FINAL.join(one_hot8, lsuffix = '_LATERALITY')

one_hot9 = pd.get_dummies(glioma_FINAL['LINK_NUMBER'])
df = glioma_FINAL.drop('LINK_NUMBER', axis = 1)
df = glioma_FINAL.join(one_hot9, lsuffix = '_LINK_NUMBER')

one_hot10 = pd.get_dummies(glioma_FINAL['MORPH_ICD10_O2'])
df = glioma_FINAL.drop('MORPH_ICD10_O2', axis = 1)
df = glioma_FINAL.join(one_hot10, lsuffix = '_MORPH_ICD10_O2')

one_hot11 = pd.get_dummies(glioma_FINAL['PERFORMANCESTATUS'])
df = glioma_FINAL.drop('PERFORMANCESTATUS', axis = 1)
df = glioma_FINAL.join(one_hot11, lsuffix = '_PERFORMANCESTATUS')

one_hot12 = pd.get_dummies(glioma_FINAL['QUINTILE_2015'])
df = glioma_FINAL.drop('QUINTILE_2015', axis = 1)
df = glioma_FINAL.join(one_hot12, lsuffix = '_QUINTILE_2015')

one_hot13 = pd.get_dummies(glioma_FINAL['SEX_y'])
df = glioma_FINAL.drop('SEX_y', axis = 1)
df = glioma_FINAL.join(one_hot13, lsuffix = '_SEX')

one_hot14 = pd.get_dummies(glioma_FINAL['SITE_ICD10_O2'])
df = glioma_FINAL.drop('SITE_ICD10_O2', axis = 1)
df = glioma_FINAL.join(one_hot14, lsuffix = '_SITE_ICD10_O2')

one_hot15 = pd.get_dummies(glioma_FINAL['SITE_ICD10_O2_3CHAR'])
df = glioma_FINAL.drop('SITE_ICD10_O2_3CHAR', axis = 1)
df = glioma_FINAL.join(one_hot15, lsuffix = '_SITE_ICD10_O2_3CHAR')

one_hot16 = pd.get_dummies(glioma_FINAL['STAGE_BEST'])
df = glioma_FINAL.drop('STAGE_BEST', axis = 1)
df = glioma_FINAL.join(one_hot16, lsuffix = '_STAGE_BEST')

one_hot18 = pd.get_dummies(glioma_FINAL['ETHNICITY'])
df = glioma_FINAL.drop('ETHNICITY', axis = 1)
df = glioma_FINAL.join(one_hot18, lsuffix = '_ETHNICITY')

one_hot19 = pd.get_dummies(glioma_FINAL['DEATHCAUSECODE_1A'])
df = glioma_FINAL.drop('DEATHCAUSECODE_1A', axis = 1)
df = glioma_FINAL.join(one_hot19, lsuffix = '_DEATHCAUSECODE_1A')

one_hot21 = pd.get_dummies(glioma_FINAL['DEATHCAUSECODE_2'])
df = glioma_FINAL.drop('DEATHCAUSECODE_2', axis = 1)
df = glioma_FINAL.join(one_hot21, lsuffix = '_DEATHCAUSECODE_2')

one_hot22 = pd.get_dummies(glioma_FINAL['DEATHCAUSECODE_UNDERLYING'])
df = glioma_FINAL.drop('DEATHCAUSECODE_UNDERLYING', axis = 1)
df = glioma_FINAL.join(one_hot22, lsuffix = '_DEATHCAUSECODE_UNDERLYING')

one_hot23 = pd.get_dummies(glioma_FINAL['DEATHLOCATIONCODE'])
df = glioma_FINAL.drop('DEATHLOCATIONCODE', axis = 1)
df = glioma_FINAL.join(one_hot23, lsuffix = '_DEATHLOCATIONCODE')

one_hot24 = pd.get_dummies(glioma_FINAL['NEWVITALSTATUS'])
df = glioma_FINAL.drop('NEWVITALSTATUS', axis = 1)
df = glioma_FINAL.join(one_hot24, lsuffix = '_NEWVITALSTATUS')

one_hot25 = pd.get_dummies(glioma_FINAL['VITALSTATUSDATE'])
df = glioma_FINAL.drop('VITALSTATUSDATE', axis = 1)
df = glioma_FINAL.join(one_hot25, lsuffix = '_VITALSTATUSDATE')

one_hot26 = pd.get_dummies(glioma_FINAL['TUMOURID'])
df = glioma_FINAL.drop('TUMOURID', axis = 1)
df = glioma_FINAL.join(one_hot26, lsuffix = '_TUMOURID')

one_hot27 = pd.get_dummies(glioma_FINAL['DIAGNOSISDATEBEST'])
df = glioma_FINAL.drop('DIAGNOSISDATEBEST', axis = 1)
df = glioma_FINAL.join(one_hot27, lsuffix = '_DIAGNOSISDATEBEST')

one_hot29 = pd.get_dummies(glioma_FINAL['AGE'])
df = glioma_FINAL.drop('AGE', axis = 1)
df = glioma_FINAL.join(one_hot29, lsuffix = '_AGE')

one_hot39 = pd.get_dummies(glioma_FINAL['DATE_FIRST_SURGERY'])
df = glioma_FINAL.drop('DATE_FIRST_SURGERY', axis = 1)
df = glioma_FINAL.join(one_hot39, lsuffix = '_DATE_FIRST_SURGERY')

one_hot41 = pd.get_dummies(glioma_FINAL['MERGED_PATIENT_ID'])
df = glioma_FINAL.drop('MERGED_PATIENT_ID', axis = 1)
df = glioma_FINAL.join(one_hot41, lsuffix = '_MERGED_PATIENT_ID')

one_hot43 = pd.get_dummies(glioma_FINAL['CONSULTANT_SPECIALITY_CODE'])
df = glioma_FINAL.drop('CONSULTANT_SPECIALITY_CODE', axis = 1)
df = glioma_FINAL.join(one_hot43, lsuffix = '_CONSULTANT_SPECIALITY_CODE')

one_hot44 = pd.get_dummies(glioma_FINAL['PRIMARY_DIAGNOSIS'])
df = glioma_FINAL.drop('PRIMARY_DIAGNOSIS', axis = 1)
df = glioma_FINAL.join(one_hot44, lsuffix = '_PRIMARY_DIAGNOSIS')

one_hot45 = pd.get_dummies(glioma_FINAL['MORPHOLOGY_CLEAN'])
df = glioma_FINAL.drop('MORPHOLOGY_CLEAN', axis = 1)
df = glioma_FINAL.join(one_hot45, lsuffix = '_MORPHOLOGY_CLEAN')

one_hot46 = pd.get_dummies(glioma_FINAL['HEIGHT_AT_START_OF_REGIMEN'])
df = glioma_FINAL.drop('HEIGHT_AT_START_OF_REGIMEN', axis = 1)
df = glioma_FINAL.join(one_hot46, lsuffix = '_HEIGHT_AT_START_OF_REGIMEN')

one_hot47 = pd.get_dummies(glioma_FINAL['WEIGHT_AT_START_OF_REGIMEN'])
df = glioma_FINAL.drop('WEIGHT_AT_START_OF_REGIMEN', axis = 1)
df = glioma_FINAL.join(one_hot47, lsuffix = '_WEIGHT_AT_START_OF_REGIMEN')

one_hot48 = pd.get_dummies(glioma_FINAL['INTENT_OF_TREATMENT'])
df = glioma_FINAL.drop('INTENT_OF_TREATMENT', axis = 1)
df = glioma_FINAL.join(one_hot48, lsuffix = '_INTENT_OF_TREATMENT')

one_hot49 = pd.get_dummies(glioma_FINAL['DATE_DECISION_TO_TREAT'])
df = glioma_FINAL.drop('DATE_DECISION_TO_TREAT', axis = 1)
df = glioma_FINAL.join(one_hot49, lsuffix = '_DATE_DECISION_TO_TREAT')

one_hot50 = pd.get_dummies(glioma_FINAL['START_DATE_OF_REGIMEN'])
df = glioma_FINAL.drop('START_DATE_OF_REGIMEN', axis = 1)
df = glioma_FINAL.join(one_hot50, lsuffix = '_START_DATE_OF_REGIMEN')

one_hot51 = pd.get_dummies(glioma_FINAL['MAPPED_REGIMEN'])
df = glioma_FINAL.drop('MAPPED_REGIMEN', axis = 1)
df = glioma_FINAL.join(one_hot51, lsuffix = '_MAPPED_REGIMEN')

one_hot52 = pd.get_dummies(glioma_FINAL['CLINICAL_TRIAL'])
df = glioma_FINAL.drop('CLINICAL_TRIAL', axis = 1)
df = glioma_FINAL.join(one_hot52, lsuffix = '_CLINICAL_TRIAL')

one_hot54 = pd.get_dummies(glioma_FINAL['BENCHMARK_GROUP'])
df = glioma_FINAL.drop('BENCHMARK_GROUP', axis = 1)
df = glioma_FINAL.join(one_hot54, lsuffix = '_BENCHMARK_GROUP')

one_hot55 = pd.get_dummies(glioma_FINAL['MERGED_OUTCOME_ID'])
df = glioma_FINAL.drop('MERGED_OUTCOME_ID', axis = 1)
df = glioma_FINAL.join(one_hot55, lsuffix = '_MERGED_OUTCOME_ID')

one_hot56 = pd.get_dummies(glioma_FINAL['REGIMEN_MOD_DOSE_REDUCTION'])
df = glioma_FINAL.drop('REGIMEN_MOD_DOSE_REDUCTION', axis = 1)
df = glioma_FINAL.join(one_hot56, lsuffix = '_REGIMEN_MOD_DOSE_REDUCTION')

one_hot57 = pd.get_dummies(glioma_FINAL['MERGED_DRUG_DETAIL_ID'])
df = glioma_FINAL.drop('MERGED_DRUG_DETAIL_ID', axis = 1)
df = glioma_FINAL.join(one_hot57, lsuffix = '_MERGED_DRUG_DETAIL_ID')

one_hot58 = pd.get_dummies(glioma_FINAL['MERGED_CYCLE_ID'])
df = glioma_FINAL.drop('MERGED_CYCLE_ID', axis = 1)
df = glioma_FINAL.join(one_hot58, lsuffix = '_MERGED_CYCLE_ID')

one_hot59 = pd.get_dummies(glioma_FINAL['ORG_CODE_OF_DRUG_PROVIDER'])
df = glioma_FINAL.drop('ORG_CODE_OF_DRUG_PROVIDER', axis = 1)
df = glioma_FINAL.join(one_hot59, lsuffix = '_ORG_CODE_OF_DRUG_PROVIDER')

one_hot60 = pd.get_dummies(glioma_FINAL['OPCS_DELIVERY_CODE'])
df = glioma_FINAL.drop('OPCS_DELIVERY_CODE', axis = 1)
df = glioma_FINAL.join(one_hot60, lsuffix = '_OPCS_DELIVERY_CODE')

one_hot61 = pd.get_dummies(glioma_FINAL['ACTUAL_DOSE_PER_ADMINISTRATION'])
df = glioma_FINAL.drop('ACTUAL_DOSE_PER_ADMINISTRATION', axis = 1)
df = glioma_FINAL.join(one_hot61, lsuffix = '_ACTUAL_DOSE_PER_ADMINISTRATION')

one_hot62 = pd.get_dummies(glioma_FINAL['HAD_SURGERY'])
df = glioma_FINAL.drop('HAD_SURGERY', axis = 1)
df = glioma_FINAL.join(one_hot62, lsuffix = '_HAD_SURGERY')

one_hot63 = pd.get_dummies(glioma_FINAL['ADMINISTRATION_ROUTE'])
df = glioma_FINAL.drop('ADMINISTRATION_ROUTE', axis = 1)
df = glioma_FINAL.join(one_hot63, lsuffix = '_ADMINISTRATION_ROUTE')

one_hot64 = pd.get_dummies(glioma_FINAL['REGIMEN_MOD_TIME_DELAY'])
df = glioma_FINAL.drop('REGIMEN_MOD_TIME_DELAY', axis = 1)
df = glioma_FINAL.join(one_hot56, lsuffix = '_REGIMEN_MOD_TIME_DELAY')

one_hot65 = pd.get_dummies(glioma_FINAL['REGIMEN_MOD_STOPPED_EARLY'])
df = glioma_FINAL.drop('REGIMEN_MOD_STOPPED_EARLY', axis = 1)
df = glioma_FINAL.join(one_hot56, lsuffix = '_REGIMEN_MOD_STOPPED_EARLY')

one_hot66 = pd.get_dummies(glioma_FINAL['ADMINISTRATION_DATE'])
df = glioma_FINAL.drop('ADMINISTRATION_DATE', axis = 1)
df = glioma_FINAL.join(one_hot66, lsuffix = '_ADMINISTRATION_DATE')

one_hot67 = pd.get_dummies(glioma_FINAL['DRUG_GROUP'])
df = glioma_FINAL.drop('DRUG_GROUP', axis = 1)
df = glioma_FINAL.join(one_hot67, lsuffix = '_DRUG_GROUP')

one_hot68 = pd.get_dummies(glioma_FINAL['MERGED_TUMOUR_ID'])
df = glioma_FINAL.drop('MERGED_TUMOUR_ID', axis = 1)
df = glioma_FINAL.join(one_hot68, lsuffix = '_MERGED_TUMOUR_ID')

one_hot69 = pd.get_dummies(glioma_FINAL['MERGED_REGIMEN_ID'])
df = glioma_FINAL.drop('MERGED_REGIMEN_ID', axis = 1)
df = glioma_FINAL.join(one_hot69, lsuffix = '_MERGED_REGIMEN_ID')

one_hot70 = pd.get_dummies(glioma_FINAL['OPCS_CODE_OF_PROVIDER'])
df = glioma_FINAL.drop('OPCS_CODE_OF_PROVIDER', axis = 1)
df = glioma_FINAL.join(one_hot70, lsuffix = '_OPCS_CODE_OF_PROVIDER')

print(df)



glioma_FINAL.to_csv("GLIOMA_TMZ.csv")

#Make a small set to work with
# LCP_Small = glioma_CP_reduced.head(100)
# LCP_Small.to_pickle("LCP_Small")
# GCP_Red_ChemoReg_Small = glioma_CP_Red_ChemoReg.head(100)
# GCP_Red_ChemoReg_Small.to_pickle("GCP_Red_Chemo")
# LCP_Small = pd.read_pickle("LCP_Small")
# patients = pd.read_csv(pat_file)
# tumours = pd.read_csv(tum_file, low_memory=False)
# regimen = pd.read_csv(regimen_file, low_memory=False, encoding = "ISO-8859-1")
# small_patients = patients.head(3)
# small_tumours = tumours.head(3)
# small_regimen = regimen.head(3)
# small_patients.to_pickle("Small_Pat")
# small_tumours.to_pickle("Small_Tum")
# small_regimen.to_pickle("Small_reg")
# small_patients = pd.read_pickle("Small_Pat")
# small_tumours = pd.read_pickle("Small_Tum")
# small_regimen = pd.read_pickle("Small_reg")
# LCP_Small = pd.read_pickle("LCP_Small")



#print(LCP_Small.count())
#print(LCP_Small.VITALSTATUSDATE.unique())
#print(LCP_Small.groupby(['GRADE']).sum())
