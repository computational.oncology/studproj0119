#A basic Simulacrum script to demonstrate some usage
#Matt Williams 2018
#matthew.williams@imperial.ac.uk

import pandas as pd
import datetime
from lifelines import KaplanMeierFitter
from lifelines.datasets import load_dd
import numpy as np
import time
data = load_dd()
#print(data.sample(6))

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_rows', 1000000)

pat_file = "sim_av_patient.csv"
tum_file = "sim_av_tumour.csv"
regimen_file = "sim_sact_regimen.csv"
sactPatient_file = "sim_sact_patient.csv"
sactTumour_file = "sim_sact_tumour.csv"
patients = pd.read_csv(pat_file)
tumours = pd.read_csv(tum_file, low_memory = False)
regimen = pd.read_csv(regimen_file, low_memory = False, encoding = "ISO-8859-1")
sactPat = pd.read_csv(sactPatient_file, low_memory = False, encoding = "ISO-8859-1")
sactTum = pd.read_csv(sactTumour_file, low_memory = False, encoding = "ISO-8859-1")

drug = pd.read_csv("sim_sact_drug_detail.csv")
outcome = pd.read_csv("sim_sact_outcome.csv")

print("*******************")

glioma = tumours[tumours['SITE_ICD10_O2_3CHAR'] == 'C71']
#print(glioma.count())
grp_behaviour = glioma.groupby(['MORPH_ICD10_O2','BEHAVIOUR_ICD10_O2'])
#print(grp_behaviour['TUMOURID'].count())

glioma_malignant = glioma[glioma['BEHAVIOUR_ICD10_O2'] == '3']
malig_grp = glioma_malignant.groupby(['MORPH_ICD10_O2'])
#print(malig_grp['TUMOURID'].count())

#print(patients.head(3))
#print(glioma_malignant.head(3))
#Now we merge data on glioma cancers and patients
#And then drop breast and prostate cancer scores
glioma_cancer_patients = pd.merge(patients, glioma_malignant, on = 'PATIENTID')
glioma_CP_reduced = glioma_cancer_patients.drop(["GLEASON_TERTIARY", "GLEASON_COMBINED", "ER_SCORE",   "PR_SCORE", "ER_STATUS", "PR_STATUS", "GLEASON_PRIMARY",  "GLEASON_SECONDARY", "HER2_STATUS", "SCREENINGSTATUSFULL_CODE", "T_BEST", "N_BEST", "M_BEST"], axis = 1)

#print("\n GLIOMA CP \n", glioma_CP_reduced.columns)
#print("\n REG: \n", regimen.columns)
#print(regimen.head(3))
# print("\n Patient: \n", patients.columns)
# print("\n SACTPAT: \n", sactPat.columns)
# print("\n Tumour: \n", tumours.columns)
# print("\n SACTTUM: \n", sactTum.columns)
# print("\n SACTReg: \n", regimen.columns)
#print("\n SACTReg: \n", regimen.groupby(["MAPPED_REGIMEN"]).sum()) #Too long to be usefuls

#print("\n Glioma_CP_Reduced: \n", glioma_CP_reduced.columns)
#Now we link our glioma patients with SACTPat
glioma_SACTLink = pd.merge(glioma_CP_reduced, sactPat, how = "left", left_on = ['LINKNUMBER_x'], right_on = ['LINK_NUMBER'])
#print("\n Glioma_SACTPAT: \n", glioma_SACTLink.columns)

#Now merge SACTTum and SACTReg
SACTTum_Reg = pd.merge(sactTum, regimen, how = "left", on = "MERGED_TUMOUR_ID")
#print("\n SACTTum_Reg: \n", SACTTum_Reg.columns)
#print("\n SACTTum_Reg: \n", SACTTum_Reg.head(3))
#print("\n SACTTum_Reg: \n", SACTTum_Reg.groupby(["PRIMARY_DIAGNOSIS"]).sum())

#Brain codes
BrainC = ("c713", "C710", "C711", "C712", "C713", "C714", "C715", "C716", "C717", "C718", "C719")
SACTTum_Reg_Brain = SACTTum_Reg[SACTTum_Reg['PRIMARY_DIAGNOSIS'].isin(BrainC)]
SACTTum_Reg_Brain.to_csv("SACTTum_Reg_BRain.csv")
#SACTTum_Reg_Brain_TEMO = SACTTum_Reg_Brain[SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("TEMOZ", case = False) == True] #Need to handle y and Y and 2
SACTTum_Reg_Brain_TEMO = SACTTum_Reg_Brain[SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("TEMOZ", case = False) == True]

##new drugs – PCV (procarbazine + lomustine (CCNU) + vincristine)
SACTTum_Reg_Brain_PC = SACTTum_Reg_Brain[SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("PROCARB", case = False) == True]
SACTTum_Reg_Brain_CCNU = SACTTum_Reg_Brain[SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("LOMUS", case = False) == True]
SACTTum_Reg_Brain_VINC = SACTTum_Reg_Brain[SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("VINCRIS", case = False) == True]

#SACTTum_Reg_Brain_TEMO_PCV = [(SACTTum_Reg_Brain["MAPPED_REGIMEN"] == str.contains("PROCARB", case = False) == True) & (SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("LOMUS", case = False) == True) & (SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("VINCRIS", case = False) == True) & (SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("TEMOZ", case = False) == True)]

# print("UNIQUE VALUES")
# print("\n procarbazine \n")
# print(SACTTum_Reg_Brain_PC['MAPPED_REGIMEN'].unique())
# print("\n lomustine / CCNU \n")
# print(SACTTum_Reg_Brain_CCNU['MAPPED_REGIMEN'].unique())
# print("\n vincristine \n")
# print(SACTTum_Reg_Brain_VINC['MAPPED_REGIMEN'].unique())
# print("\n TMZ \n")
# print(SACTTum_Reg_Brain_TEMO['MAPPED_REGIMEN'].unique())

SACTTum_Reg_Brain_TEMO_PCV = SACTTum_Reg_Brain.loc[SACTTum_Reg_Brain['MAPPED_REGIMEN'].isin(['LOMUSTINE + PROCARBAZINE' 'CARBOPLATIN + Procarbazine', 'CARBOPLATIN + CISPLATIN + CYCLOPHOSPHAMIDE + ETOPOSIDE + Procarbazine + VINCRISTINE', 'Procarbazine' 'Procarbazine + VINCRISTINE', 'Lomustine', 'LOMUSTINE + PROCARBAZINE', 'CNS Medbl HR Maint vinc+ Lomustine', 'CISPLATIN + LOMUSTINE + VINCRISTINE', 'Bevacizumab + LOMUSTINE', 'CARBOPLATIN + LOMUSTINE + VINCRISTINE', 'LOMUSTINE + VINCRISTINE', 'CARBOPLATIN + VINCRISTINE', 'Cyclophosphamide + Vincristine', 'VINCRISTINE', 'METHOTREXATE + VINCRISTINE' 'Vincristine (weekly)', 'CARBOPLATIN + CISPLATIN + CYCLOPHOSPHAMIDE + ETOPOSIDE + Procarbazine + VINCRISTINE', 'CARBO + ETOPOSIDE + VINCRISTINE', 'CARBOPLATIN + CYCLOPHOSPHAMIDE + ETOPOSIDE + FOLINIC ACID + METHOTREXATE + VINCRISTINE', 'CISPLATIN + LOMUSTINE + VINCRISTINE', 'Dactinomycin + Vincristine', 'CISPLATIN + CYCLOPHOSPHAMIDE + VINCRISTINE', 'Packer Induction Vincristine + RT', 'DACTINOMYCIN + CYCLO + VINCRISTINE', 'CARBOPLATIN + LOMUSTINE + VINCRISTINE', 'DACTINOMYCIN + IFOSFAMIDE + MESNA + VINCRISTINE', 'Procarbazine + VINCRISTINE', 'CARBOPLATIN + ETOPOSIDE + VINCRISTINE SULPHATE', 'CYCLOPHOSPHAMIDE + DACTINOMYCIN + VINCRISTINE', 'LOMUSTINE + VINCRISTINE' 'Temozolomide + RT', 'TEMOZOLOMIDE', 'Temozolomide 200mg/m2', 'CNS HGG Temozolomide +XRT' 'Temozolomide 150mg/m2', 'Temozolomide 100mg/m2 21/28day', 'CNS HGG HERBY Maint Temoz+Bev', 'EW Rel  Irinotecan & Temozolomide', 'CNS HGG HERBY Temoz+ Bev+XRT', 'CISPLATIN + TEMOZOLOMIDE', 'CNS HGG Temozolomide maintenance', 'CNS HGG HERBY Temozolomide+XRT', 'Bevacizumab+temoz+RT (weeks 1 to 6)', 'Irinotecan + Temozolomide', 'CNS HGG HERBY Maint Temozolomide', 'Bevacizumab + Temozolomide', 'CAPECITABINE + TEMOZOLOMIDE'])]

#print(df.loc[df['B'].isin(['one','three'])])

#SACTTum_Reg_Brain_TEMO_PCV = pd.merge(SACTTum_Reg_Brain_TEMO, SACTTum_Reg_Brain_PC, how = "left", on = "MERGED_TUMOUR_ID")
#SACTTum_Reg_Brain_TEMO_PCV = pd.merge(SACTTum_Reg_Brain_TEMO_PCV, SACTTum_Reg_Brain_CCNU, how = "left", on = "MERGED_TUMOUR_ID")
#SACTTum_Reg_Brain_TEMO_PCV = pd.merge(SACTTum_Reg_Brain_TEMO_PCV, SACTTum_Reg_Brain_VINC, how = "left", on = "MERGED_TUMOUR_ID")

SACTTum_Reg_Brain_TEMO_PCV.to_csv("TMZ_BRAIN_LATEST.csv")

# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.columns)
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.head(3))
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.groupby(["PRIMARY_DIAGNOSIS"]).sum())
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.groupby(["MAPPED_REGIMEN"]).sum())
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.groupby(["CHEMO_RADIATION"]).sum())

# print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.columns)
# print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.head(3))
# print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.groupby(["PRIMARY_DIAGNOSIS"]).sum())
# print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.groupby(["MAPPED_REGIMEN"]).sum())



# print("\n Glioma_SACTLink: \n", glioma_SACTLink.columns)
# print("\n Glioma_SACTLink: \n", glioma_SACTLink.head(3))
#
# #Now we merge the glioma_SACTlink with SACT_Reg_Brain_TEMO
glioma_CP_Red_TEMOReg = pd.merge(glioma_SACTLink, SACTTum_Reg_Brain_TEMO_PCV, how = "inner", left_on = ['PATIENTID'], right_on = ['MERGED_PATIENT_ID_x'])


# print("\n Glioma CP Red_TEMOReg: \n", glioma_CP_Red_TEMOReg.columns)
# print(glioma_CP_Red_TEMOReg.head(3))

outcome_drug = pd.merge(outcome, drug,  how = 'left', on = 'MERGED_PATIENT_ID')
# print("OUTCOME_DRUG: \n")
# print(outcome_drug.columns)
# print(outcome_drug.head(10))
# print(outcome_drug.isnull().sum())
glioma_FINAL_TMZ = pd.merge(glioma_CP_Red_TEMOReg, outcome_drug, how = 'left', on = 'MERGED_PATIENT_ID')

#Adding Jag & Anaïs' work here
#CLEAN SOME STUFF
glioma_FINAL_TMZ.drop(['SEX_x', 'LINKNUMBER_x', 'LINKNUMBER_y', 'MERGED_REGIMEN_ID_x','MERGED_TUMOUR_ID_x', 'MERGED_TUMOUR_ID_y','MERGED_REGIMEN_ID_y', 'MERGED_PATIENT_ID_x', 'MERGED_PATIENT_ID_y', 'CNS', 'CANCERCAREPLANINTENT', 'QUINTILE_2015', 'HEIGHT_AT_START_OF_REGIMEN', 'WEIGHT_AT_START_OF_REGIMEN', 'INTENT_OF_TREATMENT', 'ADMINISTRATION_ROUTE', 'REGIMEN_MOD_TIME_DELAY', 'REGIMEN_MOD_STOPPED_EARLY', 'REGIMEN_MOD_DOSE_REDUCTION'], axis = 1, inplace = True)
glioma_FINAL_TMZ.drop(['OPCS_DELIVERY_CODE', 'ACTUAL_DOSE_PER_ADMINISTRATION', 'STAGE_BEST', 'ORG_CODE_OF_DRUG_PROVIDER', 'DEATHCAUSECODE_1B', 'DEATHCAUSECODE_1C', 'DEATHCAUSECODE_2', 'STAGE_BEST_SYSTEM', 'REGIMEN_OUTCOME_SUMMARY', 'ACE27', 'PERFORMANCESTATUS', 'MERGED_CYCLE_ID', 'ADMINISTRATION_DATE', 'DATE_OF_FINAL_TREATMENT', 'MERGED_OUTCOME_ID', 'MERGED_DRUG_DETAIL_ID', 'MERGED_TUMOUR_ID', 'CREG_CODE'], axis = 1, inplace = True)

#print('Checking the number of missing values before')
#print('MISSING DATA BEFORE:', glioma_FINAL_TMZ.isnull().sum())

#CREATING A NEW VARIABLE --> HAD_SURGERY
glioma_FINAL_TMZ['DIAGNOSISDATEBEST'] = pd.to_datetime(glioma_FINAL_TMZ['DIAGNOSISDATEBEST'])
glioma_FINAL_TMZ['DATE_FIRST_SURGERY'] = pd.to_datetime(glioma_FINAL_TMZ['DATE_FIRST_SURGERY'])
#glioma_FINAL_TMZ['HAD_SURGERY'] = (glioma_FINAL_TMZ['DIAGNOSISDATEBEST'] - glioma_FINAL_TMZ['DATE_FIRST_SURGERY']) < np.timedelta64(3, "M")
# #MW Edit here
glioma_FINAL_TMZ['HAD_SURGERY'] = glioma_FINAL_TMZ['DATE_FIRST_SURGERY'].notnull()
glioma_FINAL_TMZ.drop(['DATE_FIRST_SURGERY'], axis = 1, inplace = True)

glioma_FINAL_TMZ['ETHNICITY'] = glioma_FINAL_TMZ['ETHNICITY'].fillna('Z')


#CREATING A NEW VARIABLE --> INTERVAL
glioma_FINAL_TMZ['VITALSTATUSDATE'] = pd.to_datetime(glioma_FINAL_TMZ['VITALSTATUSDATE'])
glioma_FINAL_TMZ['INTERVAL'] = (glioma_FINAL_TMZ['VITALSTATUSDATE'] - glioma_FINAL_TMZ['DIAGNOSISDATEBEST'])
glioma_FINAL_TMZ.drop(['VITALSTATUSDATE'], axis = 1, inplace = True)

#From Sally Vernon sally.vernon@phe.gov.uk document - Brain_Grouping
#C729 means unknwown location in the CNS
glioma_FINAL_TMZ['SITE_ICD10_O2'] = glioma_FINAL_TMZ['SITE_ICD10_O2'].fillna('C729')

#Anaïs cheat sheet
#GX means grade of differentiation is not appropriate or cannot be assessed
glioma_FINAL_TMZ['GRADE'] = glioma_FINAL_TMZ['GRADE'].fillna('GX')

def recodeCRT(value):
    if value == "None":
        return "N"
    elif value == "N":
        return value
    elif type(value) == str:
        return value.upper()
    else: return value

glioma_FINAL_TMZ['CHEMO_RADIATION'] = glioma_FINAL_TMZ['CHEMO_RADIATION'].apply(recodeCRT)
glioma_FINAL_TMZ['CHEMO_RADIATION'] = glioma_FINAL_TMZ['CHEMO_RADIATION'].fillna('N')
glioma_FINAL_TMZ['CLINICAL_TRIAL'] = glioma_FINAL_TMZ['CLINICAL_TRIAL'].fillna('N')
glioma_FINAL_TMZ['DRUG_GROUP'] = glioma_FINAL_TMZ['DRUG_GROUP'].fillna('None')

#REMOVE??
#glioma_FINAL_TMZ['DATE_FIRST_SURGERY'] = glioma_FINAL_TMZ['DATE_FIRST_SURGERY'].fillna(glioma_FINAL_TMZ['DIAGNOSISDATEBEST'] + datetime.timedelta(days = 3, hours = 10.25))

#CREATING A NEW VARIABLE --> DIFFERENCE BETWEEN DECISION TO TREAT AND ACTUALLY STARTING TREATMENT
glioma_FINAL_TMZ['DATE_DECISION_TO_TREAT'] = pd.to_datetime(glioma_FINAL_TMZ['DATE_DECISION_TO_TREAT'])
glioma_FINAL_TMZ['START_DATE_OF_REGIMEN'] = pd.to_datetime(glioma_FINAL_TMZ['START_DATE_OF_REGIMEN'])
glioma_FINAL_TMZ['DIFF_BETWEEN_DECISION_REGIMEN'] = glioma_FINAL_TMZ['START_DATE_OF_REGIMEN'] - glioma_FINAL_TMZ['DATE_DECISION_TO_TREAT']
glioma_FINAL_TMZ['DIFF_BETWEEN_DECISION_REGIMEN'] = glioma_FINAL_TMZ['DIFF_BETWEEN_DECISION_REGIMEN'].fillna('Not applicable')
glioma_FINAL_TMZ.drop(['DATE_DECISION_TO_TREAT'], axis = 1, inplace = True)
glioma_FINAL_TMZ.drop(['START_DATE_OF_REGIMEN'], axis = 1, inplace = True)

#FILLING IN WHAT WE DON'T KNOW WITH "9"
glioma_FINAL_TMZ['MORPHOLOGY_CLEAN'] = glioma_FINAL_TMZ['MORPHOLOGY_CLEAN'].fillna("9")
glioma_FINAL_TMZ['DEATHLOCATIONCODE'] = glioma_FINAL_TMZ['DEATHLOCATIONCODE'].fillna("9")
glioma_FINAL_TMZ['DEATHCAUSECODE_UNDERLYING'] = glioma_FINAL_TMZ['DEATHCAUSECODE_UNDERLYING'].fillna("9")
glioma_FINAL_TMZ['DEATHCAUSECODE_1A'] = glioma_FINAL_TMZ['DEATHCAUSECODE_1A'].fillna("9")

#SACTTum_Reg_Brain_TEMO = SACTTum_Reg_Brain[SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("TEMOZ", case = False) == True]

indexNames = glioma_FINAL_TMZ[glioma_FINAL_TMZ['NEWVITALSTATUS'].str.contains("Potential") ].index
indexNames2 = glioma_FINAL_TMZ[glioma_FINAL_TMZ['NEWVITALSTATUS'].str.contains("X") ].index
glioma_FINAL_TMZ.drop(indexNames , inplace=True)
glioma_FINAL_TMZ.drop(indexNames2 , inplace=True)

# glioma_FINAL_TMZ = glioma_FINAL_TMZ.drop(glioma_FINAL_TMZ[(glioma_FINAL_TMZ["NEWVITALSTATUS"].str.contains("Potential") == False))].index, inplace = True)

print('MISSING DATA NEW:', glioma_FINAL_TMZ.isnull().sum())
print('Values for VITALSTATUS:', glioma_FINAL_TMZ.NEWVITALSTATUS.unique())

#ADJUVANT TMZ
glioma_FINAL_TMZ['MAPPED_REGIMEN2'] = glioma_FINAL_TMZ['MAPPED_REGIMEN']
glioma_FINAL_TMZ['MAPPED_REGIMEN2'] = glioma_FINAL_TMZ['MAPPED_REGIMEN2'].replace('Temozolomide 200mg/m2', 'Adjuvant Temozolomide')
glioma_FINAL_TMZ['MAPPED_REGIMEN2'] = glioma_FINAL_TMZ['MAPPED_REGIMEN2'].replace('Temozolomide 150mg/m2', 'Adjuvant Temozolomide')

#TEMOZOLOMIDE
glioma_FINAL_TMZ['MAPPED_REGIMEN2'] = glioma_FINAL_TMZ['MAPPED_REGIMEN2'].replace('CNS HGG HERBY Temoz+ Bev+XRT', 'Temozolomide + RT')
glioma_FINAL_TMZ['MAPPED_REGIMEN2'] = glioma_FINAL_TMZ['MAPPED_REGIMEN2'].replace('CNS HGG HERBY Temozolomide+XRT', 'Temozolomide + RT')
glioma_FINAL_TMZ['MAPPED_REGIMEN2'] = glioma_FINAL_TMZ['MAPPED_REGIMEN2'].replace('Bevacizumab+temoz+RT (weeks 1 to 6)', 'Temozolomide + RT')
glioma_FINAL_TMZ['MAPPED_REGIMEN2'] = glioma_FINAL_TMZ['MAPPED_REGIMEN2'].replace('CNS HGG Temozolomide +XRT', 'Temozolomide + RT')

print("\n MAPPED REGIMEN 2", glioma_FINAL_TMZ['MAPPED_REGIMEN2'].unique())

#CHECKING THE ACCURACY OF THE CHEMO_RADIATION COLUMN
# with a function & if / for loops
#doesn't work, returns everything as correct

# def checkCHEMO(column):
#     for value in glioma_FINAL_TMZ['MAPPED_REGIMEN2']:
#         if value == 'Temozolomide + RT':
#             for value in glioma_FINAL_TMZ['CHEMO_RADIATION']:
#                 if value == 'N':
#                     return("Wrong")
#                 else:
#                     return("Correct")
#         else:
#             for value in glioma_FINAL_TMZ['CHEMO_RADIATION']:
#                 if value == 'Z':
#                     return("False")
#                 else:
#                     return("Correct")

# glioma_FINAL_TMZ['CHECK'] = glioma_FINAL_TMZ.apply(checkCHEMO, axis = 1)
# print("CHECK", glioma_FINAL_TMZ["CHECK"].unique())

# with np.where
#seems to work, none of the 'TMZ + RT' have been marked as 'Y' in 'CHEMO_RADIATION'

#Temozolomide + RT
glioma_FINAL_TMZ["CHECK_CHEMO"] = np.where((glioma_FINAL_TMZ['MAPPED_REGIMEN2'] == 'Temozolomide + RT') & (glioma_FINAL_TMZ['CHEMO_RADIATION'] == 'Y'), 'Correct', 'Wrong')
print("CHECK CHEMO_RADIATION 1", glioma_FINAL_TMZ["CHECK_CHEMO"].unique())

#everything else
glioma_FINAL_TMZ["CHECK_CHEMO2"] = np.where((glioma_FINAL_TMZ['MAPPED_REGIMEN2'] != 'Temozolomide + RT') & (glioma_FINAL_TMZ['CHEMO_RADIATION'] == 'N'), 'Correct', 'Wrong')
print("CHECK CHEMO_RADIATION 2", glioma_FINAL_TMZ["CHECK_CHEMO2"].unique())

#rewriting the CHEMO_RADIATION column
print("% % % % % % % % % % % % % % % % % %")
glioma_FINAL_TMZ["CHEMO_RADIATION_NEW"] = glioma_FINAL_TMZ['MAPPED_REGIMEN2']
glioma_FINAL_TMZ['CHEMO_RADIATION_NEW'] = glioma_FINAL_TMZ['MAPPED_REGIMEN2'].replace('Temozolomide + RT', 'Y')
glioma_FINAL_TMZ['CHEMO_RADIATION_NEW'] = glioma_FINAL_TMZ['CHEMO_RADIATION_NEW'].replace('Packer Induction Vincristine + RT', 'Y')
glioma_FINAL_TMZ['CHEMO_RADIATION_NEW'] = np.where((glioma_FINAL_TMZ['CHEMO_RADIATION_NEW'] != 'Y'), 'N', 'Y')

#glioma_FINAL_TMZ['CHEMO_RADIATION_NEW'] = np.where((glioma_FINAL_TMZ['MAPPED_REGIMEN2'] == 'Temozolomide + RT'), 'Y', 'N')
#print(glioma_FINAL_TMZ['CHEMO_RADIATION_NEW'].unique())

# glioma_FINAL_TMZ['CHEMO_RADIATION_NEW2'] = np.where((glioma_FINAL_TMZ['MAPPED_REGIMEN2'] == glioma_FINAL_TMZ['MAPPED_REGIMEN2'].str.contains("+ RT")), 'Y', 'N')

#ONE HOT ENCODING
# one_hot2 = pd.get_dummies(glioma_FINAL_TMZ['BEHAVIOUR_ICD10_O2'])
# df = glioma_FINAL_TMZ.drop('BEHAVIOUR_ICD10_O2', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot2, lsuffix = '_BEHAVIOUR_ICD10_O2')
#
# one_hot5 = pd.get_dummies(glioma_FINAL_TMZ['CREG_CODE'])
# df = glioma_FINAL_TMZ.drop('CREG_CODE', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot5, lsuffix = '_CREG_CODE')
#
# one_hot7 = pd.get_dummies(glioma_FINAL_TMZ['GRADE'])
# df = glioma_FINAL_TMZ.drop('GRADE', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot7, lsuffix = '_GRADE')
#
# one_hot8 = pd.get_dummies(glioma_FINAL_TMZ['LATERALITY'])
# df = glioma_FINAL_TMZ.drop('LATERALITY', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot8, lsuffix = '_LATERALITY')
#
# one_hot9 = pd.get_dummies(glioma_FINAL_TMZ['LINK_NUMBER'])
# df = glioma_FINAL_TMZ.drop('LINK_NUMBER', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot9, lsuffix = '_LINK_NUMBER')
#
# one_hot10 = pd.get_dummies(glioma_FINAL_TMZ['MORPH_ICD10_O2'])
# df = glioma_FINAL_TMZ.drop('MORPH_ICD10_O2', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot10, lsuffix = '_MORPH_ICD10_O2')
#
# one_hot13 = pd.get_dummies(glioma_FINAL_TMZ['SEX_y'])
# df = glioma_FINAL_TMZ.drop('SEX_y', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot13, lsuffix = '_SEX')
#
# one_hot14 = pd.get_dummies(glioma_FINAL_TMZ['SITE_ICD10_O2'])
# df = glioma_FINAL_TMZ.drop('SITE_ICD10_O2', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot14, lsuffix = '_SITE_ICD10_O2')
#
# one_hot15 = pd.get_dummies(glioma_FINAL_TMZ['SITE_ICD10_O2_3CHAR'])
# df = glioma_FINAL_TMZ.drop('SITE_ICD10_O2_3CHAR', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot15, lsuffix = '_SITE_ICD10_O2_3CHAR')
#
# one_hot18 = pd.get_dummies(glioma_FINAL_TMZ['ETHNICITY'])
# df = glioma_FINAL_TMZ.drop('ETHNICITY', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot18, lsuffix = '_ETHNICITY')
#
# one_hot22 = pd.get_dummies(glioma_FINAL_TMZ['DEATHCAUSECODE_UNDERLYING'])
# df = glioma_FINAL_TMZ.drop('DEATHCAUSECODE_UNDERLYING', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot22, lsuffix = '_DEATHCAUSECODE_UNDERLYING')
#
# one_hot23 = pd.get_dummies(glioma_FINAL_TMZ['DEATHLOCATIONCODE'])
# df = glioma_FINAL_TMZ.drop('DEATHLOCATIONCODE', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot23, lsuffix = '_DEATHLOCATIONCODE')
#
# one_hot24 = pd.get_dummies(glioma_FINAL_TMZ['NEWVITALSTATUS'])
# df = glioma_FINAL_TMZ.drop('NEWVITALSTATUS', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot24, lsuffix = '_NEWVITALSTATUS')
#
# one_hot25 = pd.get_dummies(glioma_FINAL_TMZ['VITALSTATUSDATE'])
# df = glioma_FINAL_TMZ.drop('VITALSTATUSDATE', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot25, lsuffix = '_VITALSTATUSDATE')
#
# one_hot26 = pd.get_dummies(glioma_FINAL_TMZ['TUMOURID'])
# df = glioma_FINAL_TMZ.drop('TUMOURID', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot26, lsuffix = '_TUMOURID')
#
# one_hot27 = pd.get_dummies(glioma_FINAL_TMZ['DIAGNOSISDATEBEST'])
# df = glioma_FINAL_TMZ.drop('DIAGNOSISDATEBEST', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot27, lsuffix = '_DIAGNOSISDATEBEST')
#
# one_hot29 = pd.get_dummies(glioma_FINAL_TMZ['AGE'])
# df = glioma_FINAL_TMZ.drop('AGE', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot29, lsuffix = '_AGE')
#
# one_hot39 = pd.get_dummies(glioma_FINAL_TMZ['DATE_FIRST_SURGERY'])
# df = glioma_FINAL_TMZ.drop('DATE_FIRST_SURGERY', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot39, lsuffix = '_DATE_FIRST_SURGERY')
#
# one_hot41 = pd.get_dummies(glioma_FINAL_TMZ['MERGED_PATIENT_ID'])
# df = glioma_FINAL_TMZ.drop('MERGED_PATIENT_ID', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot41, lsuffix = '_MERGED_PATIENT_ID')
#
# one_hot43 = pd.get_dummies(glioma_FINAL_TMZ['CONSULTANT_SPECIALITY_CODE'])
# df = glioma_FINAL_TMZ.drop('CONSULTANT_SPECIALITY_CODE', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot43, lsuffix = '_CONSULTANT_SPECIALITY_CODE')
#
# one_hot44 = pd.get_dummies(glioma_FINAL_TMZ['PRIMARY_DIAGNOSIS'])
# df = glioma_FINAL_TMZ.drop('PRIMARY_DIAGNOSIS', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot44, lsuffix = '_PRIMARY_DIAGNOSIS')
#
# one_hot45 = pd.get_dummies(glioma_FINAL_TMZ['MORPHOLOGY_CLEAN'])
# df = glioma_FINAL_TMZ.drop('MORPHOLOGY_CLEAN', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot45, lsuffix = '_MORPHOLOGY_CLEAN')
#
# one_hot49 = pd.get_dummies(glioma_FINAL_TMZ['DATE_DECISION_TO_TREAT'])
# df = glioma_FINAL_TMZ.drop('DATE_DECISION_TO_TREAT', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot49, lsuffix = '_DATE_DECISION_TO_TREAT')
#
# one_hot50 = pd.get_dummies(glioma_FINAL_TMZ['START_DATE_OF_REGIMEN'])
# df = glioma_FINAL_TMZ.drop('START_DATE_OF_REGIMEN', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot50, lsuffix = '_START_DATE_OF_REGIMEN')
#
# one_hot51 = pd.get_dummies(glioma_FINAL_TMZ['MAPPED_REGIMEN'])
# df = glioma_FINAL_TMZ.drop('MAPPED_REGIMEN', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot51, lsuffix = '_MAPPED_REGIMEN')
#
# one_hot54 = pd.get_dummies(glioma_FINAL_TMZ['BENCHMARK_GROUP'])
# df = glioma_FINAL_TMZ.drop('BENCHMARK_GROUP', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot54, lsuffix = '_BENCHMARK_GROUP')
#
# one_hot55 = pd.get_dummies(glioma_FINAL_TMZ['MERGED_OUTCOME_ID'])
# df = glioma_FINAL_TMZ.drop('MERGED_OUTCOME_ID', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot55, lsuffix = '_MERGED_OUTCOME_ID')
#
# one_hot57 = pd.get_dummies(glioma_FINAL_TMZ['MERGED_DRUG_DETAIL_ID'])
# df = glioma_FINAL_TMZ.drop('MERGED_DRUG_DETAIL_ID', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot57, lsuffix = '_MERGED_DRUG_DETAIL_ID')
#
# one_hot58 = pd.get_dummies(glioma_FINAL_TMZ['MERGED_CYCLE_ID'])
# df = glioma_FINAL_TMZ.drop('MERGED_CYCLE_ID', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot58, lsuffix = '_MERGED_CYCLE_ID')
#
# one_hot58 = pd.get_dummies(glioma_FINAL_TMZ['DEATHCAUSECODE_1A'])
# df = glioma_FINAL_TMZ.drop('DEATHCAUSECODE_1A', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot58, lsuffix = '_DEATHCAUSECODE_1A')
#
# one_hot62 = pd.get_dummies(glioma_FINAL_TMZ['HAD_SURGERY'])
# df = glioma_FINAL_TMZ.drop('HAD_SURGERY', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot62, lsuffix = '_HAD_SURGERY')
#
# one_hot63 = pd.get_dummies(glioma_FINAL_TMZ['CHEMO_RADIATION'])
# df = glioma_FINAL_TMZ.drop('CHEMO_RADIATION', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot63, lsuffix = '_CHEMO_RADIATION')
#
# one_hot66 = pd.get_dummies(glioma_FINAL_TMZ['ADMINISTRATION_DATE'])
# df = glioma_FINAL_TMZ.drop('ADMINISTRATION_DATE', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot66, lsuffix = '_ADMINISTRATION_DATE')
#
# one_hot67 = pd.get_dummies(glioma_FINAL_TMZ['DRUG_GROUP'])
# df = glioma_FINAL_TMZ.drop('DRUG_GROUP', axis = 1, inplace = True)
# df = glioma_FINAL_TMZ.join(one_hot67, lsuffix = '_DRUG_GROUP')

#print(df.head(10))
#print(SACTTum_Reg_Brain_TEMO.columns)
#print(SACTTum_Reg_Brain_TEMO.head(100))

#SACTTum_Reg_Brain_TEMO.to_csv("SACTTum_Reg_Brain_TEMO.csv")
#print(df['VITALSTATUSDATE'])
glioma_FINAL_TMZ_MALIG = glioma_FINAL_TMZ[(glioma_FINAL_TMZ.GRADE == "G3") | (glioma_FINAL_TMZ.GRADE == "G4")]
print("#######################\n", glioma_FINAL_TMZ_MALIG.columns)
glioma_FINAL_TMZ_MALIG_SHORT = glioma_FINAL_TMZ_MALIG.drop(["DEATHCAUSECODE_UNDERLYING", "TUMOURID","CONSULTANT_SPECIALITY_CODE", "DIAGNOSISDATEBEST", "BENCHMARK_GROUP", "DRUG_GROUP","LINK_NUMBER","DEATHLOCATIONCODE", "BEHAVIOUR_ICD10_O2","DEATHLOCATIONCODE", "DIFF_BETWEEN_DECISION_REGIMEN", "MERGED_REGIMEN_ID"], axis = 1)

#print("$$$$$$$$$$$$$$$$$$$$$$$$")
#print(glioma_FINAL_TMZ['MAPPED_REGIMEN'].unique())

#finding duplicates
#print(glioma_FINAL_TMZ["PATIENTID"].duplicated(keep = "first"))

#glioma_FINAL_TMZ.drop_duplicates(subset = "PATIENTID", keep = "first", inplace = True)
glioma_FINAL_TMZ.drop_duplicates(subset = ['PATIENTID', 'DRUG_GROUP'], keep = 'first', inplace = True)

print("SHAPE", glioma_FINAL_TMZ.shape)

glioma_FINAL_TMZ_MALIG_SHORT.to_csv("GLIOMA_TMZ_MALIG_SHORT.csv")
glioma_FINAL_TMZ.to_csv("GLIOMA_TMZ_PCV_LATEST4.csv")

#Make a small set to work with
# LCP_Small = glioma_CP_reduced.head(100)
# LCP_Small.to_pickle("LCP_Small")
# GCP_Red_ChemoReg_Small = glioma_CP_Red_ChemoReg.head(100)
# GCP_Red_ChemoReg_Small.to_pickle("GCP_Red_Chemo")
# LCP_Small = pd.read_pickle("LCP_Small")
# patients = pd.read_csv(pat_file)
# tumours = pd.read_csv(tum_file, low_memory=False)
# regimen = pd.read_csv(regimen_file, low_memory=False, encoding = "ISO-8859-1")
# small_patients = patients.head(3)
# small_tumours = tumours.head(3)
# small_regimen = regimen.head(3)
# small_patients.to_pickle("Small_Pat")
# small_tumours.to_pickle("Small_Tum")
# small_regimen.to_pickle("Small_reg")
# small_patients = pd.read_pickle("Small_Pat")
# small_tumours = pd.read_pickle("Small_Tum")
# small_regimen = pd.read_pickle("Small_reg")
# LCP_Small = pd.read_pickle("LCP_Small")



#print(LCP_Small.count())
#print(LCP_Small.VITALSTATUSDATE.unique())
#print(LCP_Small.groupby(['GRADE']).sum())
