import pandas as pd
import datetime
import numpy as np

#df = pd.read_csv('GLIOMA_TMZ_2.csv')
df = pd.read_csv('GLIOMA_TMZ.csv')

df.drop(['SEX_x', 'LINKNUMBER_x', 'LINKNUMBER_y', 'MERGED_REGIMEN_ID_x','MERGED_TUMOUR_ID_x', 'MERGED_TUMOUR_ID_y','MERGED_REGIMEN_ID_y', 'MERGED_PATIENT_ID_x', 'MERGED_PATIENT_ID_y'], axis = 1, inplace = True)
print(df.columns)

print('Checking the number of missing values before')
print('MISSING DATA:', df.isnull().sum())

#Based on the missing data we decided to drop the following
df.drop(['DEATHCAUSECODE_1B', 'DEATHCAUSECODE_1C', 'STAGE_BEST_SYSTEM', 'REGIMEN_OUTCOME_SUMMARY'], axis = 1, inplace = True)
print(df.columns)

#CREATING A NEW VARIABLE --> HAD_SURGERY
df['DIAGNOSISDATEBEST'] = pd.to_datetime(df['DIAGNOSISDATEBEST'])
df['DATE_FIRST_SURGERY'] = pd.to_datetime(df['DATE_FIRST_SURGERY'])

df['HAD_SURGERY'] = (df['DIAGNOSISDATEBEST'] - df['DATE_FIRST_SURGERY']) > np.timedelta64(3, "M")

#FILLING IN MISSING DATA
df['ETHNICITY'] = df['ETHNICITY'].fillna('Z')

#From Sally Vernon sally.vernon@phe.gov.uk document - Brain_Grouping
#C729 means unknwown location in the CNS
df['SITE_ICD10_O2'] = df['SITE_ICD10_O2'].fillna('C729')

#Anais cheat sheet
#GX means grade of differentiation is not appropriate or cannot be assessed
df['GRADE'] = df['GRADE'].fillna('GX')

#https://www.datadictionary.nhs.uk/data_dictionary/data_field_notes/c/cancer_care_plan_intent_de.asp?shownav=1
# 9 = not known
df['CANCERCAREPLANINTENT'] = df['CANCERCAREPLANINTENT'].fillna('9')

df['CNS'] = df['CNS'].fillna('99')
#df['CNS'] = df['CNS'].replace('Y1m', 'Y1')

df['ACE27'] = df['ACE27'].fillna('9')

#over16 = df.loc[df['AGE'] > 16]
df['PERFORMANCESTATUS'] = df['PERFORMANCESTATUS'].fillna('9')
#df['PERFORMANCESTATUS'] = df['PERFORMANCESTATUS'].replace('1m', 1)
#df['PERFORMANCESTATUS'] = df['PERFORMANCESTATUS'].astype(str)

#QUINTILE_2015
df['QUINTILE_2015'] = df['QUINTILE_2015'].replace('5 - most deprived', 5)
df['QUINTILE_2015'] = df['QUINTILE_2015'].replace('1 - least deprived', 1)
df['QUINTILE_2015'] = df['QUINTILE_2015'].astype(int)

#ADDING DATES TO DATE_FIRST_SURGERY
diff = df['DATE_FIRST_SURGERY'] - df['DIAGNOSISDATEBEST']
#print("diff1", diff.mean())
#print("diff1", diff.unique())
df['DATE_FIRST_SURGERY'] = df['DATE_FIRST_SURGERY'].fillna(df['DIAGNOSISDATEBEST'] + datetime.timedelta(days = 3, hours = 10.25))

#HEIGHT_AT_START_OF_REGIMEN
#right now, only replacing missing heights with mean
height_mean = df['HEIGHT_AT_START_OF_REGIMEN'].mean()
df['HEIGHT_AT_START_OF_REGIMEN'] = df['HEIGHT_AT_START_OF_REGIMEN'].fillna(height_mean)
#print("STANDARD DEVIATION HEIGHT", np.std(df['HEIGHT_AT_START_OF_REGIMEN']))

#Below: heights by age / sex --> values are very close

#adult_female = df.loc[(df['AGE'] > 16) & (df['SEX_y'] == 2)]
#height_meanF = adult_female['HEIGHT_AT_START_OF_REGIMEN'].mean()
#print("ADULT FEMALE", height_meanF)

#adult_male = df.loc[(df['AGE'] > 16) & (df['SEX_y'] == 1)]
#height_meanM = adult_male['HEIGHT_AT_START_OF_REGIMEN'].mean()
#print("ADULT MALE", height_meanM)

#children_female = df.loc[(df['AGE'] <= 16) & (df['SEX_y'] == 2)]
#height_meanCF = children_female['HEIGHT_AT_START_OF_REGIMEN'].mean()
#print("CHILDREN FEMALE", height_meanCF)

#children_male = df.loc[(df['AGE'] <= 16) & (df['SEX_y'] == 1)]
#height_meanCM = children_male['HEIGHT_AT_START_OF_REGIMEN'].mean()
#print("MALE CHILDREN", height_meanCM)

#WEIGHT_AT_START_OF_REGIMEN
#right now, only replacing missing weights with mean
#will find means by age / sex later
#standard is high --> will change later
weight_mean = df['WEIGHT_AT_START_OF_REGIMEN'].mean()
df['WEIGHT_AT_START_OF_REGIMEN'] = df['WEIGHT_AT_START_OF_REGIMEN'].fillna(weight_mean)
#print("STANDARD DEVIATION WEIGHT", np.std(df['WEIGHT_AT_START_OF_REGIMEN']))

#drug treatment intent
df['INTENT_OF_TREATMENT'] = df['INTENT_OF_TREATMENT'].fillna("9")
df['INTENT_OF_TREATMENT'] = df['INTENT_OF_TREATMENT'].replace(4, "9")

df['CLINICAL_TRIAL'] = df['CLINICAL_TRIAL'].fillna("99")

df['ADMINISTRATION_ROUTE'] = df['ADMINISTRATION_ROUTE'].fillna("99")

#ADDING DATES TO DATE_DECISION_TO_TREAT
df['DATE_DECISION_TO_TREAT'] = pd.to_datetime(df['DATE_DECISION_TO_TREAT'])
df['START_DATE_OF_REGIMEN'] = pd.to_datetime(df['START_DATE_OF_REGIMEN'])
diff2 = df['START_DATE_OF_REGIMEN'] - df['DATE_DECISION_TO_TREAT']
#print("diff2", diff2.mean())
#print("diff2", diff2.unique())
df['DATE_DECISION_TO_TREAT'] = df['DATE_DECISION_TO_TREAT'].fillna(df['START_DATE_OF_REGIMEN'] - datetime.timedelta(days = 21, hours = 8.42))

#ADDING DATES TO DATE_OF_FINAL_TREATMENT
df['DATE_OF_FINAL_TREATMENT'] = pd.to_datetime(df['DATE_OF_FINAL_TREATMENT'])
diff3 = df['DATE_OF_FINAL_TREATMENT'] - df['START_DATE_OF_REGIMEN']
#print("diff3", diff3.mean())
#print("diff3", diff3.unique())
df['DATE_OF_FINAL_TREATMENT'] = df['DATE_OF_FINAL_TREATMENT'].fillna(df['START_DATE_OF_REGIMEN'] + datetime.timedelta(days = 88))

#dropping CHEMO_RADIATION because contradicts DRUG_GROUP
df.drop(['CHEMO_RADIATION'], axis = 1, inplace = True)

#ADMINISTRATION_DATE
#relevant?
df['ADMINISTRATION_DATE'] = pd.to_datetime(df['ADMINISTRATION_DATE'])
diff4 = df['ADMINISTRATION_DATE'] - df['START_DATE_OF_REGIMEN']
#print("diff4", diff4.mean())
#print("diff4", diff4.unique())
df['ADMINISTRATION_DATE'] = df['ADMINISTRATION_DATE'].fillna(df['START_DATE_OF_REGIMEN'] + datetime.timedelta(days = 81, hours = 4.7))


#FILLING IN WHAT WE DON'T KNOW WITH "9"
df['REGIMEN_MOD_DOSE_REDUCTION'] = df['REGIMEN_MOD_DOSE_REDUCTION'].fillna("9")
df['REGIMEN_MOD_STOPPED_EARLY'] = df['REGIMEN_MOD_STOPPED_EARLY'].fillna("9")
df['REGIMEN_MOD_TIME_DELAY'] = df['REGIMEN_MOD_TIME_DELAY'].fillna("9")
df['OPCS_CODE_OF_PROVIDER'] = df['REGIMEN_MOD_TIME_DELAY'].fillna("9")
df['OPCS_DELIVERY_CODE'] = df['OPCS_DELIVERY_CODE'].fillna("9")
df['MORPHOLOGY_CLEAN'] = df['MORPHOLOGY_CLEAN'].fillna("9")
df['DEATHLOCATIONCODE'] = df['DEATHLOCATIONCODE'].fillna("9")
df['DEATHCAUSECODE_UNDERLYING'] = df['DEATHCAUSECODE_UNDERLYING'].fillna("9")
df['DEATHCAUSECODE_2'] = df['DEATHCAUSECODE_2'].fillna("9")
df['DEATHCAUSECODE_1A'] = df['DEATHCAUSECODE_1A'].fillna("9")

df['ACTUAL_DOSE_PER_ADMINISTRATION'] = df['ACTUAL_DOSE_PER_ADMINISTRATION'].fillna("XXX")

print('MISSING DATA:', df.isnull().sum())

df = df.applymap(str)

#for value in df['PATIENTID']:
#    print(type(value))
#    break

#ONE HOT ENCODING
one_hot1 = pd.get_dummies(df['ACE27'])
df = df.drop('ACE27', axis = 1)
df = df.join(one_hot1, lsuffix = '_ACE27')

one_hot2 = pd.get_dummies(df['BEHAVIOUR_ICD10_O2'])
df = df.drop('BEHAVIOUR_ICD10_O2', axis = 1)
df = df.join(one_hot2, lsuffix = '_BEHAVIOUR_ICD10_O2')

one_hot3 = pd.get_dummies(df['CANCERCAREPLANINTENT'])
df = df.drop('CANCERCAREPLANINTENT', axis = 1)
df = df.join(one_hot3, lsuffix = '_CANCERCAREPLANINTENT')

one_hot4 = pd.get_dummies(df['CNS'])
df = df.drop('CNS', axis = 1)
df = df.join(one_hot4, lsuffix = '_CANCERCAREPLANINTENT')

one_hot5 = pd.get_dummies(df['CREG_CODE'])
df = df.drop('CREG_CODE', axis = 1)
df = df.join(one_hot5, lsuffix = '_CREG_CODE')

one_hot7 = pd.get_dummies(df['GRADE'])
df = df.drop('GRADE', axis = 1)
df = df.join(one_hot7, lsuffix = '_GRADE')

one_hot8 = pd.get_dummies(df['LATERALITY'])
df = df.drop('LATERALITY', axis = 1)
df = df.join(one_hot8, lsuffix = '_LATERALITY')

one_hot9 = pd.get_dummies(df['LINK_NUMBER'])
df = df.drop('LINK_NUMBER', axis = 1)
df = df.join(one_hot9, lsuffix = '_LINK_NUMBER')

one_hot10 = pd.get_dummies(df['MORPH_ICD10_O2'])
df = df.drop('MORPH_ICD10_O2', axis = 1)
df = df.join(one_hot10, lsuffix = '_MORPH_ICD10_O2')

one_hot11 = pd.get_dummies(df['PERFORMANCESTATUS'])
df = df.drop('PERFORMANCESTATUS', axis = 1)
df = df.join(one_hot11, lsuffix = '_PERFORMANCESTATUS')

one_hot12 = pd.get_dummies(df['QUINTILE_2015'])
df = df.drop('QUINTILE_2015', axis = 1)
df = df.join(one_hot12, lsuffix = '_QUINTILE_2015')

one_hot13 = pd.get_dummies(df['SEX_y'])
df = df.drop('SEX_y', axis = 1)
df = df.join(one_hot13, lsuffix = '_SEX')

one_hot14 = pd.get_dummies(df['SITE_ICD10_O2'])
df = df.drop('SITE_ICD10_O2', axis = 1)
df = df.join(one_hot14, lsuffix = '_SITE_ICD10_O2')

one_hot15 = pd.get_dummies(df['SITE_ICD10_O2_3CHAR'])
df = df.drop('SITE_ICD10_O2_3CHAR', axis = 1)
df = df.join(one_hot15, lsuffix = '_SITE_ICD10_O2_3CHAR')

one_hot16 = pd.get_dummies(df['STAGE_BEST'])
df = df.drop('STAGE_BEST', axis = 1)
df = df.join(one_hot16, lsuffix = '_STAGE_BEST')

one_hot18 = pd.get_dummies(df['ETHNICITY'])
df = df.drop('ETHNICITY', axis = 1)
df = df.join(one_hot18, lsuffix = '_ETHNICITY')

one_hot19 = pd.get_dummies(df['DEATHCAUSECODE_1A'])
df = df.drop('DEATHCAUSECODE_1A', axis = 1)
df = df.join(one_hot19, lsuffix = '_DEATHCAUSECODE_1A')

one_hot21 = pd.get_dummies(df['DEATHCAUSECODE_2'])
df = df.drop('DEATHCAUSECODE_2', axis = 1)
df = df.join(one_hot21, lsuffix = '_DEATHCAUSECODE_2')

one_hot22 = pd.get_dummies(df['DEATHCAUSECODE_UNDERLYING'])
df = df.drop('DEATHCAUSECODE_UNDERLYING', axis = 1)
df = df.join(one_hot22, lsuffix = '_DEATHCAUSECODE_UNDERLYING')

one_hot23 = pd.get_dummies(df['DEATHLOCATIONCODE'])
df = df.drop('DEATHLOCATIONCODE', axis = 1)
df = df.join(one_hot23, lsuffix = '_DEATHLOCATIONCODE')

one_hot24 = pd.get_dummies(df['NEWVITALSTATUS'])
df = df.drop('NEWVITALSTATUS', axis = 1)
df = df.join(one_hot24, lsuffix = '_NEWVITALSTATUS')

one_hot25 = pd.get_dummies(df['VITALSTATUSDATE'])
df = df.drop('VITALSTATUSDATE', axis = 1)
df = df.join(one_hot25, lsuffix = '_VITALSTATUSDATE')

one_hot26 = pd.get_dummies(df['TUMOURID'])
df = df.drop('TUMOURID', axis = 1)
df = df.join(one_hot26, lsuffix = '_TUMOURID')

one_hot27 = pd.get_dummies(df['DIAGNOSISDATEBEST'])
df = df.drop('DIAGNOSISDATEBEST', axis = 1)
df = df.join(one_hot27, lsuffix = '_DIAGNOSISDATEBEST')

one_hot29 = pd.get_dummies(df['AGE'])
df = df.drop('AGE', axis = 1)
df = df.join(one_hot29, lsuffix = '_AGE')

one_hot39 = pd.get_dummies(df['DATE_FIRST_SURGERY'])
df = df.drop('DATE_FIRST_SURGERY', axis = 1)
df = df.join(one_hot39, lsuffix = '_DATE_FIRST_SURGERY')

one_hot41 = pd.get_dummies(df['MERGED_PATIENT_ID'])
df = df.drop('MERGED_PATIENT_ID', axis = 1)
df = df.join(one_hot41, lsuffix = '_MERGED_PATIENT_ID')

one_hot43 = pd.get_dummies(df['CONSULTANT_SPECIALITY_CODE'])
df = df.drop('CONSULTANT_SPECIALITY_CODE', axis = 1)
df = df.join(one_hot43, lsuffix = '_CONSULTANT_SPECIALITY_CODE')

one_hot44 = pd.get_dummies(df['PRIMARY_DIAGNOSIS'])
df = df.drop('PRIMARY_DIAGNOSIS', axis = 1)
df = df.join(one_hot44, lsuffix = '_PRIMARY_DIAGNOSIS')

one_hot45 = pd.get_dummies(df['MORPHOLOGY_CLEAN'])
df = df.drop('MORPHOLOGY_CLEAN', axis = 1)
df = df.join(one_hot45, lsuffix = '_MORPHOLOGY_CLEAN')

one_hot46 = pd.get_dummies(df['HEIGHT_AT_START_OF_REGIMEN'])
df = df.drop('HEIGHT_AT_START_OF_REGIMEN', axis = 1)
df = df.join(one_hot46, lsuffix = '_HEIGHT_AT_START_OF_REGIMEN')

one_hot47 = pd.get_dummies(df['WEIGHT_AT_START_OF_REGIMEN'])
df = df.drop('WEIGHT_AT_START_OF_REGIMEN', axis = 1)
df = df.join(one_hot47, lsuffix = '_WEIGHT_AT_START_OF_REGIMEN')

one_hot48 = pd.get_dummies(df['INTENT_OF_TREATMENT'])
df = df.drop('INTENT_OF_TREATMENT', axis = 1)
df = df.join(one_hot48, lsuffix = '_INTENT_OF_TREATMENT')

one_hot49 = pd.get_dummies(df['DATE_DECISION_TO_TREAT'])
df = df.drop('DATE_DECISION_TO_TREAT', axis = 1)
df = df.join(one_hot49, lsuffix = '_DATE_DECISION_TO_TREAT')

one_hot50 = pd.get_dummies(df['START_DATE_OF_REGIMEN'])
df = df.drop('START_DATE_OF_REGIMEN', axis = 1)
df = df.join(one_hot50, lsuffix = '_START_DATE_OF_REGIMEN')

one_hot51 = pd.get_dummies(df['MAPPED_REGIMEN'])
df = df.drop('MAPPED_REGIMEN', axis = 1)
df = df.join(one_hot51, lsuffix = '_MAPPED_REGIMEN')

one_hot52 = pd.get_dummies(df['CLINICAL_TRIAL'])
df = df.drop('CLINICAL_TRIAL', axis = 1)
df = df.join(one_hot52, lsuffix = '_CLINICAL_TRIAL')

one_hot54 = pd.get_dummies(df['BENCHMARK_GROUP'])
df = df.drop('BENCHMARK_GROUP', axis = 1)
df = df.join(one_hot54, lsuffix = '_BENCHMARK_GROUP')

one_hot55 = pd.get_dummies(df['MERGED_OUTCOME_ID'])
df = df.drop('MERGED_OUTCOME_ID', axis = 1)
df = df.join(one_hot55, lsuffix = '_MERGED_OUTCOME_ID')

one_hot56 = pd.get_dummies(df['REGIMEN_MOD_DOSE_REDUCTION'])
df = df.drop('REGIMEN_MOD_DOSE_REDUCTION', axis = 1)
df = df.join(one_hot56, lsuffix = '_REGIMEN_MOD_DOSE_REDUCTION')

one_hot57 = pd.get_dummies(df['MERGED_DRUG_DETAIL_ID'])
df = df.drop('MERGED_DRUG_DETAIL_ID', axis = 1)
df = df.join(one_hot57, lsuffix = '_MERGED_DRUG_DETAIL_ID')

one_hot58 = pd.get_dummies(df['MERGED_CYCLE_ID'])
df = df.drop('MERGED_CYCLE_ID', axis = 1)
df = df.join(one_hot58, lsuffix = '_MERGED_CYCLE_ID')

one_hot59 = pd.get_dummies(df['ORG_CODE_OF_DRUG_PROVIDER'])
df = df.drop('ORG_CODE_OF_DRUG_PROVIDER', axis = 1)
df = df.join(one_hot59, lsuffix = '_ORG_CODE_OF_DRUG_PROVIDER')

one_hot60 = pd.get_dummies(df['OPCS_DELIVERY_CODE'])
df = df.drop('OPCS_DELIVERY_CODE', axis = 1)
df = df.join(one_hot60, lsuffix = '_OPCS_DELIVERY_CODE')

one_hot61 = pd.get_dummies(df['ACTUAL_DOSE_PER_ADMINISTRATION'])
df = df.drop('ACTUAL_DOSE_PER_ADMINISTRATION', axis = 1)
df = df.join(one_hot61, lsuffix = '_ACTUAL_DOSE_PER_ADMINISTRATION')

one_hot62 = pd.get_dummies(df['HAD_SURGERY'])
df = df.drop('HAD_SURGERY', axis = 1)
df = df.join(one_hot62, lsuffix = '_HAD_SURGERY')

one_hot63 = pd.get_dummies(df['ADMINISTRATION_ROUTE'])
df = df.drop('ADMINISTRATION_ROUTE', axis = 1)
df = df.join(one_hot63, lsuffix = '_ADMINISTRATION_ROUTE')

one_hot64 = pd.get_dummies(df['REGIMEN_MOD_TIME_DELAY'])
df = df.drop('REGIMEN_MOD_TIME_DELAY', axis = 1)
df = df.join(one_hot56, lsuffix = '_REGIMEN_MOD_TIME_DELAY')

one_hot65 = pd.get_dummies(df['REGIMEN_MOD_STOPPED_EARLY'])
df = df.drop('REGIMEN_MOD_STOPPED_EARLY', axis = 1)
df = df.join(one_hot56, lsuffix = '_REGIMEN_MOD_STOPPED_EARLY')

one_hot66 = pd.get_dummies(df['ADMINISTRATION_DATE'])
df = df.drop('ADMINISTRATION_DATE', axis = 1)
df = df.join(one_hot66, lsuffix = '_ADMINISTRATION_DATE')

one_hot67 = pd.get_dummies(df['DRUG_GROUP'])
df = df.drop('DRUG_GROUP', axis = 1)
df = df.join(one_hot67, lsuffix = '_DRUG_GROUP')

one_hot68 = pd.get_dummies(df['MERGED_TUMOUR_ID'])
df = df.drop('MERGED_TUMOUR_ID', axis = 1)
df = df.join(one_hot68, lsuffix = '_MERGED_TUMOUR_ID')

one_hot69 = pd.get_dummies(df['MERGED_REGIMEN_ID'])
df = df.drop('MERGED_REGIMEN_ID', axis = 1)
df = df.join(one_hot69, lsuffix = '_MERGED_REGIMEN_ID')

one_hot70 = pd.get_dummies(df['OPCS_CODE_OF_PROVIDER'])
df = df.drop('OPCS_CODE_OF_PROVIDER', axis = 1)
df = df.join(one_hot70, lsuffix = '_OPCS_CODE_OF_PROVIDER')

print(df)
