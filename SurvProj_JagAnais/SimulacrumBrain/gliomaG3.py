#IMPORTING THE LIBRARIES
import pandas as pd

#GETTING THE DATA
#regimen = pd.read_csv('sim_sact_regimen.csv')
#cvs file is missing regimen

df = pd.read_csv('gliomaG3.csv')
print(df)

print("GRADE .unique()")
print(df["GRADE"].unique())

print("MORPH_ICD10_O2 .unique()")
print(df["MORPH_ICD10_O2"].unique())

for column_name in df:
      print("The column name is:", column_name)
      s = df[column_name].unique()
      for i in s:
        print(str(i))
