#A basic Simulacrum script to demonstrate some usage
#Matt Williams 2018
#matthew.williams@imperial.ac.uk

import pandas as pd
from lifelines import KaplanMeierFitter
from lifelines.datasets import load_dd
import time
data = load_dd()
#print(data.sample(6))

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_rows', 1000000)

pat_file = "sim_av_patient.csv"
tum_file = "sim_av_tumour.csv"
regimen_file = "sim_sact_regimen.csv"
sactPatient_file = "sim_sact_patient.csv"
sactTumour_file = "sim_sact_tumour.csv"
patients = pd.read_csv(pat_file)
tumours = pd.read_csv(tum_file, low_memory = False)
regimen = pd.read_csv(regimen_file, low_memory = False, encoding = "ISO-8859-1")
sactPat = pd.read_csv(sactPatient_file, low_memory = False, encoding = "ISO-8859-1")
sactTum = pd.read_csv(sactTumour_file, low_memory = False, encoding = "ISO-8859-1")

drug = pd.read_csv("sim_sact_drug_detail.csv")
outcome = pd.read_csv("sim_sact_outcome.csv")

print("*******************")

glioma = tumours[tumours['SITE_ICD10_O2_3CHAR'] == 'C71']
#print(glioma.count())
grp_behaviour = glioma.groupby(['MORPH_ICD10_O2','BEHAVIOUR_ICD10_O2'])
#print(grp_behaviour['TUMOURID'].count())

glioma_malignant = glioma[glioma['BEHAVIOUR_ICD10_O2'] == '3']
malig_grp = glioma_malignant.groupby(['MORPH_ICD10_O2'])
#print(malig_grp['TUMOURID'].count())

#print(patients.head(3))
#print(glioma_malignant.head(3))
#Now we merge data on glioma cancers and patients
#And then drop breast and prostate cancer scores
glioma_cancer_patients = pd.merge(patients, glioma_malignant, on = 'PATIENTID')
glioma_CP_reduced = glioma_cancer_patients.drop(["GLEASON_TERTIARY", "GLEASON_COMBINED", "ER_SCORE",   "PR_SCORE", "ER_STATUS", "PR_STATUS", "GLEASON_PRIMARY",  "GLEASON_SECONDARY", "HER2_STATUS", "SCREENINGSTATUSFULL_CODE", "T_BEST", "N_BEST", "M_BEST"], axis = 1)

#print("\n GLIOMA CP \n", glioma_CP_reduced.columns)
#print("\n REG: \n", regimen.columns)
#print(regimen.head(3))
print("\n Patient: \n", patients.columns)
print("\n SACTPAT: \n", sactPat.columns)
print("\n Tumour: \n", tumours.columns)
print("\n SACTTUM: \n", sactTum.columns)
print("\n SACTReg: \n", regimen.columns)
#print("\n SACTReg: \n", regimen.groupby(["MAPPED_REGIMEN"]).sum()) #Too long to be usefuls

print("\n Glioma_CP_Reduced: \n", glioma_CP_reduced.columns)
#Now we link our glioma patients with SACTPat
glioma_SACTLink = pd.merge(glioma_CP_reduced, sactPat, how="inner", left_on =['LINKNUMBER_x'], right_on=['LINK_NUMBER'])
print("\n Glioma_SACTPAT: \n", glioma_SACTLink.columns)

#Now merge SACTTum and SACTReg
SACTTum_Reg = pd.merge(sactTum, regimen, how = "inner", on="MERGED_TUMOUR_ID")
#print("\n SACTTum_Reg: \n", SACTTum_Reg.columns)
#print("\n SACTTum_Reg: \n", SACTTum_Reg.head(3))
#print("\n SACTTum_Reg: \n", SACTTum_Reg.groupby(["PRIMARY_DIAGNOSIS"]).sum())

#Brain codes
BrainC = ("c713", "C710", "C711", "C712", "C713", "C714", "C715", "C716", "C717", "C718", "C719")
SACTTum_Reg_Brain = SACTTum_Reg[SACTTum_Reg['PRIMARY_DIAGNOSIS'].isin(BrainC)]
SACTTum_Reg_Brain_TEMO = SACTTum_Reg_Brain[SACTTum_Reg_Brain["MAPPED_REGIMEN"].str.contains("TEMOZ") == True] #Need to handle y and Y and 2

# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.columns)
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.head(3))
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.groupby(["PRIMARY_DIAGNOSIS"]).sum())
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.groupby(["MAPPED_REGIMEN"]).sum())
# print("\n SACTTum_Reg_Brain: \n", SACTTum_Reg_Brain.groupby(["CHEMO_RADIATION"]).sum())

print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.columns)
print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.head(3))
# print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.groupby(["PRIMARY_DIAGNOSIS"]).sum())
# print("\n SACTTum_Reg_Brain_TEMO: \n", SACTTum_Reg_Brain_TEMO.groupby(["MAPPED_REGIMEN"]).sum())

# print("\n Glioma_SACTLink: \n", glioma_SACTLink.columns)
# print("\n Glioma_SACTLink: \n", glioma_SACTLink.head(3))
#
# #Now we merge the glioma_SACTlink with SACT_Reg_Brain_TEMO
glioma_CP_Red_TEMOReg = pd.merge(glioma_SACTLink, SACTTum_Reg_Brain_TEMO, how="inner", left_on =['PATIENTID'], right_on=['MERGED_PATIENT_ID_x'])
#
print("\n Glioma CP Red_TEMOReg: \n", glioma_CP_Red_TEMOReg.columns)
print(glioma_CP_Red_TEMOReg.head(3))

outcome_drug = pd.merge(outcome, drug,  how = 'inner', on = 'MERGED_PATIENT_ID')
glioma_FINAL = pd.merge(glioma_CP_Red_TEMOReg, outcome_drug, how = 'left', on = 'MERGED_PATIENT_ID')

glioma_FINAL.to_csv("GLIOMA_TMZ_2.csv")

#Make a small set to work with
# LCP_Small = glioma_CP_reduced.head(100)
# LCP_Small.to_pickle("LCP_Small")
# GCP_Red_ChemoReg_Small = glioma_CP_Red_ChemoReg.head(100)
# GCP_Red_ChemoReg_Small.to_pickle("GCP_Red_Chemo")
# LCP_Small = pd.read_pickle("LCP_Small")
# patients = pd.read_csv(pat_file)
# tumours = pd.read_csv(tum_file, low_memory=False)
# regimen = pd.read_csv(regimen_file, low_memory=False, encoding = "ISO-8859-1")
# small_patients = patients.head(3)
# small_tumours = tumours.head(3)
# small_regimen = regimen.head(3)
# small_patients.to_pickle("Small_Pat")
# small_tumours.to_pickle("Small_Tum")
# small_regimen.to_pickle("Small_reg")
# small_patients = pd.read_pickle("Small_Pat")
# small_tumours = pd.read_pickle("Small_Tum")
# small_regimen = pd.read_pickle("Small_reg")
# LCP_Small = pd.read_pickle("LCP_Small")



#print(LCP_Small.count())
#print(LCP_Small.VITALSTATUSDATE.unique())
#print(LCP_Small.groupby(['GRADE']).sum())
