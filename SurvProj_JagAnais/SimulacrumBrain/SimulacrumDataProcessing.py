import pandas as pd
import datetime
import numpy as np
from numpy import array
import sklearn as sklearn
from sklearn import preprocessing as pp
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

df = pd.read_csv('gliomaG3.csv')

print("*******************************************")

#Dataframe before dropping the columns
#print('FULL DATAFRAME COUNT:\n', df.count())

#Dropping the columns that have NO values
df.drop(['ACTUAL_DOSE_PER_ADMINISTRATION', 'ADMINISTRATION_DATE', 'ADMINISTRATION_ROUTE',
'ADMINISTRATION_ROUTE', 'DATE_OF_FINAL_TREATMENT', 'DEATHCAUSECODE_1A', 'DEATHCAUSECODE_1B', 'DEATHCAUSECODE_1C',
'DEATHCAUSECODE_2', 'DEATHCAUSECODE_2', 'DEATHCAUSECODE_UNDERLYING', 'DEATHLOCATIONCODE', 'DRUG_GROUP', 'ER_STATUS',
'ETHNICITY', 'GLEASON_COMBINED', 'GLEASON_SECONDARY', 'GLEASON_TERTIARY', 'HER2_STATUS', 'LINK_NUMBER',
'MERGED_CYCLE_ID', 'MERGED_DRUG_DETAIL_ID', 'MERGED_OUTCOME_ID', 'MERGED_PATIENT_ID', 'MERGED_REGIMEN_ID',
'MERGED_TUMOUR_ID', 'NEWVITALSTATUS', 'OPCS_DELIVERY_CODE', 'ORG_CODE_OF_DRUG_PROVIDER', 'PR_SCORE', 'PR_STATUS',
'REGIMEN_MOD_DOSE_REDUCTION', 'REGIMEN_MOD_STOPPED_EARLY', 'REGIMEN_MOD_TIME_DELAY', 'REGIMEN_OUTCOME_SUMMARY',
'SCREENINGSTATUSFULL_CODE', 'VITALSTATUSDATE'], axis = 1, inplace = True)

#Dropping the columns that have very few vaues in them
df.drop(['ER_SCORE'], axis = 1, inplace = True)

#.count() lists each column and the number of values in each column
#print('FULL DATAFRAME COUNT AFTER DROPPING COLUMNS:\n', df.count())

#FILLING IN MISSING DATA & CONVERTING DATA
#isnull().sum() gives the number of values missing in each column
#print('MISSING DATA:', df.isnull().sum())

#ACE27
#filling in missing values with 9 (9 = unknown)
df['ACE27'] = df['ACE27'].fillna(9)
df['ACE27'] = df['ACE27'].astype(int)

#BEHAVIOUR_ICD10_O2
#converting to integers
df['BEHAVIOUR_ICD10_O2'] = df['BEHAVIOUR_ICD10_O2'].astype(int)

#CANCERCAREPLANINTENT
#9 = unknown
df['CANCERCAREPLANINTENT'] = df['CANCERCAREPLANINTENT'].fillna('9')
#df['CANCERCAREPLANINTENT'] = df['CANCERCAREPLANINTENT'].replace('9', int(9))

#CNS
#99 = unknown
df['CNS'] = df['CNS'].fillna('99')
#df['CNS'] = df['CNS'].replace('99', int(99))
df['CNS'] = df['CNS'].replace('Y1m', 'Y1')

#ADDING DATES TO DATE_FIRST_SURGERY
df['DATE_FIRST_SURGERY'] = pd.to_datetime(df['DATE_FIRST_SURGERY'])
df['DIAGNOSISDATEBEST'] = pd.to_datetime(df['DIAGNOSISDATEBEST'])
diff = df['DIAGNOSISDATEBEST'] - df['DATE_FIRST_SURGERY']
#print(diff.abs().mean())
df['DATE_FIRST_SURGERY'] = df['DATE_FIRST_SURGERY'].fillna(df['DIAGNOSISDATEBEST'] + datetime.timedelta(days = 5))

#GLEASON_PRIMARY
df['GLEASON_PRIMARY'] = df['GLEASON_PRIMARY'].fillna(8)
df['GLEASON_PRIMARY'] = df['GLEASON_PRIMARY'].astype(int)

#LATERALITY
df['LATERALITY'] = df['LATERALITY'].astype(str)

#LINKNUMBER
df['LINKNUMBER'] = df['LINKNUMBER'].astype(int)

#MORPH_ICD10_O2
df['MORPH_ICD10_O2'] = df['MORPH_ICD10_O2'].astype(int)

#M_BEST, N_BEST & T_BEST
#998 = unknown
df['M_BEST'] = df['M_BEST'].fillna(998)
df['N_BEST'] = df['N_BEST'].fillna(998)
df['T_BEST'] = df['T_BEST'].fillna(998)
df['N_BEST'] = df['N_BEST'].astype(int)
df['M_BEST'] = df['M_BEST'].astype(str)
df['T_BEST'] = df['T_BEST'].astype(str)

#PERFORMANCE STATUS
#the performance status codes change if looking at children (under 16) or adults
#9 = unknown for teenagers / adults > 16
over16 = df.loc[df['AGE'] > 16]
over16['PERFORMANCESTATUS'] = over16['PERFORMANCESTATUS'].fillna(9)
df['PERFORMANCESTATUS'] = over16['PERFORMANCESTATUS']
df['PERFORMANCESTATUS'] = df['PERFORMANCESTATUS'].replace('1m', 1)
df['PERFORMANCESTATUS'] = df['PERFORMANCESTATUS'].astype(str)

#QUINTILE_2015
df['QUINTILE_2015'] = df['QUINTILE_2015'].replace('5 - most deprived', 5)
df['QUINTILE_2015'] = df['QUINTILE_2015'].replace('1 - least deprived', 1)
df['QUINTILE_2015'] = df['QUINTILE_2015'].astype(int)

#SEX
df['SEX'] = df['SEX'].astype(int)

#STAGE_BEST_SYSTEM
#24 = unknown
df['STAGE_BEST_SYSTEM'] = df['STAGE_BEST_SYSTEM'].fillna(24)
df['STAGE_BEST_SYSTEM'] = df['STAGE_BEST_SYSTEM'].astype(int)

#SITE_ICD10_O2_3CHAR
#replacing the missing value with the 3-character code that matches the 4-character code
df['SITE_ICD10_O2_3CHAR'] = df['SITE_ICD10_O2_3CHAR'].fillna('C71')

#will probably be dropped
#PATIENTID
df['PATIENTID'] = df['PATIENTID'].astype(int)

#TUMOURID
df['TUMOURID'] = df['TUMOURID'].astype(int)

#NUMBER OF MISSING VALUES
print('NUMBER OF MISSING VALUES (after filling in missing data):\n', df.isnull().sum())

#CONVERTING EVERYTHING TO STRINGS
#does not include AGE, DATE_FIRST_SURGERY, DIAGNOSISDATEBEST, TUMOURID, PATIENTID

df['ACE27'] = df['ACE27'].astype(str)
df['BEHAVIOUR_ICD10_O2'] = df['BEHAVIOUR_ICD10_O2'].astype(str)
df['CANCERCAREPLANINTENT'] = df['CANCERCAREPLANINTENT'].astype(str)
df['CNS'] = df['CNS'].astype(str)
df['CREG_CODE'] = df['CREG_CODE'].astype(str)
df['GLEASON_PRIMARY'] = df['GLEASON_PRIMARY'].astype(str)
df['GRADE'] = df['GRADE'].astype(str)
df['LATERALITY'] = df['LATERALITY'].astype(str)
df['LINKNUMBER'] = df['LINKNUMBER'].astype(str)
df['MORPH_ICD10_O2'] = df['MORPH_ICD10_O2'].astype(str)
df['M_BEST'] = df['M_BEST'].astype(str)
df['N_BEST'] = df['N_BEST'].astype(str)
df['PERFORMANCESTATUS'] = df['PERFORMANCESTATUS'].astype(str)
df['QUINTILE_2015'] = df['QUINTILE_2015'].astype(str)
df['SEX'] = df['SEX'].astype(str)
df['SITE_ICD10_O2'] = df['SITE_ICD10_O2'].astype(str)
df['SITE_ICD10_O2_3CHAR'] = df['SITE_ICD10_O2_3CHAR'].astype(str)
df['STAGE_BEST'] = df['STAGE_BEST'].astype(str)
df['STAGE_BEST_SYSTEM'] = df['STAGE_BEST_SYSTEM'].astype(str)
df['T_BEST'] = df['T_BEST'].astype(str)

#LABELENCODER
# limit to categorical data using df.select_dtypes()
#X = df.select_dtypes(include=[object])
#le = pp.LabelEncoder()
#df1 = X.apply(le.fit_transform)
#print(df1)

#ONE HOT ENCODING
#enc = pp.OneHotEncoder(sparse = False, handle_unknown = 'ignore')
#enc.fit(df1['ACE27'])
#onehotlabels = enc.transform(df1['ACE27'])
#print("OneHotLabels")
#print(onehotlabels)
#print(type(onehotlabels))

one_hot1 = pd.get_dummies(df['ACE27'])
df = df.drop('ACE27', axis = 1)
df = df.join(one_hot1, lsuffix = '_ACE27')

one_hot2 = pd.get_dummies(df['BEHAVIOUR_ICD10_O2'])
df = df.drop('BEHAVIOUR_ICD10_O2', axis = 1)
df = df.join(one_hot2, lsuffix = '_BEHAVIOUR_ICD10_O2')

one_hot3 = pd.get_dummies(df['CANCERCAREPLANINTENT'])
df = df.drop('CANCERCAREPLANINTENT', axis = 1)
df = df.join(one_hot3, lsuffix = '_CANCERCAREPLANINTENT')

one_hot4 = pd.get_dummies(df['CNS'])
df = df.drop('CNS', axis = 1)
df = df.join(one_hot4, lsuffix = '_CANCERCAREPLANINTENT')

one_hot5 = pd.get_dummies(df['CREG_CODE'])
df = df.drop('CREG_CODE', axis = 1)
df = df.join(one_hot5, lsuffix = '_CREG_CODE')

one_hot6 = pd.get_dummies(df['GLEASON_PRIMARY'])
df = df.drop('GLEASON_PRIMARY', axis = 1)
df = df.join(one_hot6, lsuffix = '_GLEASON_PRIMARY')

one_hot7 = pd.get_dummies(df['GRADE'])
df = df.drop('GRADE', axis = 1)
df = df.join(one_hot7, lsuffix = '_GRADE')

one_hot8 = pd.get_dummies(df['LATERALITY'])
df = df.drop('LATERALITY', axis = 1)
df = df.join(one_hot8, lsuffix = '_LATERALITY')

one_hot9 = pd.get_dummies(df['LINKNUMBER'])
df = df.drop('LINKNUMBER', axis = 1)
df = df.join(one_hot9, lsuffix = '_LINKNUMBER')

one_hot10 = pd.get_dummies(df['MORPH_ICD10_O2'])
df = df.drop('MORPH_ICD10_O2', axis = 1)
df = df.join(one_hot10, lsuffix = '_MORPH_ICD10_O2')

one_hot11 = pd.get_dummies(df['M_BEST'])
df = df.drop('M_BEST', axis = 1)
df = df.join(one_hot11, lsuffix = '_M_BEST')

one_hot12 = pd.get_dummies(df['N_BEST'])
df = df.drop('N_BEST', axis = 1)
df = df.join(one_hot12, lsuffix = '_N_BEST')

one_hot13 = pd.get_dummies(df['PERFORMANCESTATUS'])
df = df.drop('PERFORMANCESTATUS', axis = 1)
df = df.join(one_hot13, lsuffix = '_PERFORMANCESTATUS')

one_hot14 = pd.get_dummies(df['QUINTILE_2015'])
df = df.drop('QUINTILE_2015', axis = 1)
df = df.join(one_hot14, lsuffix = '_QUINTILE_2015')

one_hot15 = pd.get_dummies(df['SEX'])
df = df.drop('SEX', axis = 1)
df = df.join(one_hot15, lsuffix = '_SEX')

one_hot16 = pd.get_dummies(df['SITE_ICD10_O2'])
df = df.drop('SITE_ICD10_O2', axis = 1)
df = df.join(one_hot16, lsuffix = '_SITE_ICD10_O2')

one_hot17 = pd.get_dummies(df['SITE_ICD10_O2_3CHAR'])
df = df.drop('SITE_ICD10_O2_3CHAR', axis = 1)
df = df.join(one_hot17, lsuffix = '_SITE_ICD10_O2_3CHAR')

one_hot18 = pd.get_dummies(df['STAGE_BEST'])
df = df.drop('STAGE_BEST', axis = 1)
df = df.join(one_hot18, lsuffix = '_STAGE_BEST')

one_hot19 = pd.get_dummies(df['STAGE_BEST_SYSTEM'])
df = df.drop('STAGE_BEST_SYSTEM', axis = 1)
df = df.join(one_hot19, lsuffix = '_STAGE_BEST_SYSTEM')

one_hot20 = pd.get_dummies(df['T_BEST'])
df = df.drop('T_BEST', axis = 1)
df = df.join(one_hot20, lsuffix = '_T_BEST')

#NORMALIZING DATA
#x_array = np.array(df['AGE'])
#normalized_X = pp.normalize([x_array])
#print(normalized_X)

x = df[['AGE']].values.astype(float)
min_max_scaler = pp.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(x)
df_normalized = pd.DataFrame(x_scaled)

df = df.drop('AGE', axis = 1)
df = df.join(df_normalized)
print(df)
#ages are in the "O" columns at the right


#PRINTS UNIQUE VALUES IN EACH COLUMN
#for column_name in df:
#      print('The column name is:', column_name)
#      s = df[column_name].unique()
#      for i in s:
#        print(str(i))

######################
#['CANCERCAREPLANINTENT', 'CNS', 'CREG_CODE', 'GRADE', 'LATERALITY', 'M_BEST', 'PERFORMANCESTATUS',
#'SITE_ICD10_O2_3CHAR', 'SITE_ICD10_O2', 'STAGE_BEST', 'T_BEST']
