import pandas as pd
import numpy as np
import matplotlib.pyplot as ply
from sklearn.model_selection import train_test_split
from sklearn import preprocessing as pp

d1 = pd.read_csv("titanic.csv")


le = pp.LabelEncoder()
d1["SexNum"] = le.transform(d1["Sex"])
d1["ClassNum"] = le.transform(d1["Class"])
d1["SurvNum"] = le.transform(d1["Surv"])
d1.drop["Class", "Sex", "Surv"]
print(d1)

X = d1.drop("Surv", axis=1)
Y = d1["Surv"]
print(X)
print(Y)

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.20)
from sklearn.svm import SVC
svclassifier = SVC(kernel="linear")
svclassifier.fit(X_train, Y_train)
Y_pred = svclassifier.predict(X_test)

