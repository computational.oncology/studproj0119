#IMPORTING THE LIBRARIES (1)
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import sklearn as sklearn
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn import preprocessing as pp
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC

#GETTING THE DATA (2)
#Dataset to be used to make / test the code
train = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')

#EXPLORING THE DATA (3)
print("Information about the dataset")

#Information about the dataset (types of variables, etc.)
#print(train.info())

#Gives count, mean, median, etc. for each feature
#print(train.describe())

#Prints the top 4 rows of the dataset
print("Head: \n", train.head(4))

#MISSING DATA (4)
print("Missing data")
#Gives the number of values missing in each column
MissingData1 = train.isnull().sum()
print(MissingData1)

#Gives the total number of values missing
MissingData2 = train.isnull().sum().sum()
print("Overall, there are", MissingData2, "values missing.")


titles = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}

train['Title'] = train.Name.str.extract(' ([A-Za-z]+)\.', expand=False)
# replace titles with a more common title or as Rare
print(train['Title'].unique())
train['Title'] = train['Title'].replace(['Lady', 'Countess','Capt', 'Col','Don', 'Dr',\
                                        'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')
train['Title'] = train['Title'].replace('Mlle', 'Miss')
train['Title'] = train['Title'].replace('Ms', 'Miss')
train['Title'] = train['Title'].replace('Mme', 'Mrs')
# convert titles into numbers
train['Title'] = train['Title'].map(titles)
# filling NaN with 0, to get safe
train['Title'] = train['Title'].fillna(0)
train['Title'] = train['Title'].astype(int)

print("post-titles: \n", train.head(4))

deck = {"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "G": 6, "F": 7, "T": 8}


train['Floor'] = train.Cabin.str.extract('([A-Za-z]+)', expand = False)
print(train['Floor'].unique())
train['Floor'] = train['Floor'].map(deck)
train['Floor'].fillna(0, inplace = True)
train['Floor'] = train['Floor'].astype(int)

print("Post-cabin: \n", train.head(10))

#print(train.head())
#FIXING DATA
#drop 4 columns
train.drop(['PassengerId', 'Name', 'Ticket', 'Cabin'], axis = 1, inplace = True)

#fill missing Ages with the median age
train['Age'].fillna(train['Age'].median(),inplace=True)

#fill missing embarked info with top embarked location (S)
train['Embarked'].fillna(train['Embarked'].value_counts().index[0], inplace = True)

#Creating new Features
#The importance of these features will be tested to determine which ones are most important
#Important features will be retained, unimportant features will be dropped

#Relatives
#loc slices the DataFrame
#In the exapmle below loc returns only the reltives column of the dataframe as a series
#To retuen it as a dataframe means you have to us [[]]
train['relatives'] = train['SibSp'] + train['Parch']
train.loc[train['relatives'] > 0, 'With family'] = 1
train.loc[train['relatives'] == 0, 'With family'] = 0
train['With family'] = train['With family'].astype(int)
print(train['With family'].value_counts())
print(train.head(10))

#AgeClass
train['Age_Class'] = train['Age']*train['Pclass']
print(train.head(4))

#Convert String to values for Sex & Embarked
le = pp.LabelEncoder()
le.fit(train['Embarked'])
train['EmbarkedNum'] = le.transform(train['Embarked'])
train.drop('Embarked',1,inplace = True)

le2 = pp.LabelEncoder()
le2.fit(train['Sex'])
train["SexNum"] = le2.transform(train["Sex"])
train.drop("Sex",1, inplace = True)

print("HeadFinal: \n", train.head(4))

#Normalizing Age
x_array = np.array(train['Age'])
normalized_Age = pp.normalize([x_array])
#print(normalized_Age)

x_array = np.array(train['Fare'])
normalized_Fare = pp.normalize([x_array])
#print(normalized_Fare)

#this is not working:
train['NormalizedAge'] = normalized_Age
train['NormalizedAge'] = train['NormalizedAge'].fillna(0, inplace = True)

# Splitting Features and Label
y = train['Survived']
X = train.drop(['Survived'],1)

#Using Train Test Split from Sklearn to Split Our Train Dataset into Train and Testing Datasets

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

LinModel = linear_model.LinearRegression()
LinModel.fit(X_train, y_train)
LinPredictions = LinModel.predict(X_test)

GBRmodel = GradientBoostingClassifier(learning_rate=0.1,max_depth=3)
GBRmodel.fit(X_train, y_train)
GBRpredictions = GBRmodel.predict(X_test)

RFCmodel = RandomForestClassifier(min_samples_leaf = 5)
RFCmodel.fit(X_train, y_train)
RFCpredictions = RFCmodel.predict(X_test)

print('Coefficients: \n', LinModel.coef_)
# The mean squared error
print("Mean squared error: %.2f" % mean_squared_error(y_test, LinPredictions))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(y_test, LinPredictions))

print("Conf Matrix GBR \n", confusion_matrix(y_test, GBRpredictions))
print("Class Report GBR\n", classification_report(y_test,GBRpredictions))

print("Conf Matrix RFC \n", confusion_matrix(y_test, RFCpredictions))
print("Class Report RFC\n", classification_report(y_test,RFCpredictions))

################################
#Can we tune the parameters?

tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                     'C': [1, 1000]},
                    {'kernel': ['linear'], 'C': [1, 1000]}]


scores = ['precision', 'recall']

#testing the importance of each feature
importances = pd.DataFrame({'feature':X_train.columns,'importance':np.round(RFCmodel.feature_importances_,3)})
importances = importances.sort_values('importance',ascending=False).set_index('feature')
print('TRAIN', importances.head(15))

importances = pd.DataFrame({'feature':X_test.columns,'importance':np.round(RFCmodel.feature_importances_,3)})
importances = importances.sort_values('importance',ascending=False).set_index('feature')
print('TEST', importances.head(15))

#COMPARING THE MODEL
acc_random_forest = round(RFCmodel.score(X_train, y_train) * 100, 2)
acc_gradient_booster = round(GBRmodel.score(X_train, y_train) * 100, 2)
acc_linear_regression = round(LinModel.score(X_train, y_train) * 100, 2)

results = pd.DataFrame({'Model': ['Random Forest', 'Linear Regression', "Gradient Booster"], 'Score': [acc_random_forest, acc_linear_regression, acc_gradient_booster]})
result_df = results.sort_values(by='Score', ascending=False)
result_df = result_df.set_index('Score')
print(result_df.head(9))


# for score in scores:
#     print("# Tuning hyper-parameters for %s" % score)
#     print()
#
#     clf = GridSearchCV(SVC(), tuned_parameters, cv=5, scoring='%s_macro' % score)
#     #clf = GridSearchCV(GradientBoostingClassifier(), tuned_parameters, cv=5, scoring='%s_macro' % score)
#     clf.fit(X_train, y_train)
#
#     print("Best parameters set found on development set:")
#     print()
#     print(clf.best_params_)
#     print()
#     print("Grid scores on development set:")
#     print()
#     means = clf.cv_results_['mean_test_score']
#     stds = clf.cv_results_['std_test_score']
#     for mean, std, params in zip(means, stds, clf.cv_results_['params']):
#         print("%0.3f (+/-%0.03f) for %r"
#               % (mean, std * 2, params))
#     print()
#
#     print("Detailed classification report:")
#     print()
#     print("The model is trained on the full development set.")
#     print("The scores are computed on the full evaluation set.")
#     print()
#     y_true, y_pred = y_test, clf.predict(X_test)
#     print(classification_report(y_true, y_pred))
#     print()
