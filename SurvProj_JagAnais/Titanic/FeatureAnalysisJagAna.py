#IMPORTING THE LIBRARIES
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn import tree

train = pd.read_csv('train.csv')

#Sex
train = pd.read_csv('train.csv', index_col = 0)

sns.barplot(x = 'Sex', y = 'Survived', data = train)
plt.show()

#Embarked
sns.barplot(x = 'Embarked', y = 'Survived', data = train)
plt.show()

#Pclass
sns.barplot(x = 'Pclass', y = 'Survived', data = train)
plt.show()

#Parch
sns.barplot(x = 'Parch', y = 'Survived', data = train)
plt.show()

#SibSp
sns.barplot(x = 'SibSp', y = 'Survived', data = train)
plt.show()

#Relatives - plot graph

#Age & Sex (from article)
survived = 'survived'
not_survived = 'not survived'
fig, axes = plt.subplots(nrows=1, ncols=2,figsize=(10, 4))
women = train[train['Sex']=='female']
men = train[train['Sex']=='male']
ax = sns.distplot(women[women['Survived']==1].Age.dropna(), bins=18, label = survived, ax = axes[0], kde =False)
ax = sns.distplot(women[women['Survived']==0].Age.dropna(), bins=40, label = not_survived, ax = axes[0], kde =False)
ax.legend()
ax.set_title('Female')
ax = sns.distplot(men[men['Survived']==1].Age.dropna(), bins=18, label = survived, ax = axes[1], kde = False)
ax = sns.distplot(men[men['Survived']==0].Age.dropna(), bins=40, label = not_survived, ax = axes[1], kde = False)
ax.legend()
ax.set_title('Male')
plt.show()

#plot sex / survival only
