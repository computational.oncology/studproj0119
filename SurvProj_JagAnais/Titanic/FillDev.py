"""Just a quick play to try an improve the use of missing data.
Based on using MV linear regression; could consider using Multiple Imputation as well"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import sklearn as sklearn
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn import preprocessing as pp
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC

#GETTING THE DATA (2)
#Dataset to be used to make / test the code
train = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')

#Prints the top 4 rows of the dataset
print("Head: \n", train.head(4))

#MISSING DATA (4)
print("Missing data")
#Gives the number of values missing in each column
MissingData1 = train.isnull().sum()
print(MissingData1)
print("Overall, there are", MissingData1.sum(), "values missing.")

train.drop(['PassengerId', 'Name', 'Ticket', 'Cabin'], axis = 1, inplace = True)

#fill missing embarked info with top embarked location (S)
#train['Embarked'].fillna(train['Embarked'].value_counts().index[0], inplace = True)

trainComplete = train.dropna(axis = 0).copy(deep = True)
MissingData2 = trainComplete.isnull().sum()
print(MissingData2)
print("Overall, there are now", MissingData2.sum(), "values missing.")

#Convert String to values for Sex & Embarked
le = pp.LabelEncoder()
le.fit(trainComplete['Embarked'])
trainComplete['EmbarkedNum'] = le.transform(trainComplete['Embarked'])
trainComplete.drop('Embarked',1,inplace = True)

le2 = pp.LabelEncoder()
le2.fit(trainComplete['Sex'])
trainComplete["SexNum"] = le2.transform(trainComplete["Sex"])
trainComplete.drop("Sex",1, inplace = True)

#Main missing values are Age and Cabin

y = trainComplete['Age']
X = trainComplete.drop(['Age'],1)

#Using Train Test Split from Sklearn to Split Our Train Dataset into Train and Testing Datasets

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

LinModel = linear_model.LinearRegression()
LinModel.fit(X_train, y_train)
LinPredictions = LinModel.predict(X_test)

print('Coefficients: \n', LinModel.coef_)
# The mean squared error
print("Mean squared error: %.2f" % mean_squared_error(y_test, LinPredictions))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(y_test, LinPredictions))


# #fill missing Ages with the median age
# train['Age'].fillna(train['Age'].median(),inplace=True)
#
# #fill missing embarked info with top embarked location (S)
# train['Embarked'].fillna(train['Embarked'].value_counts().index[0], inplace = True)

