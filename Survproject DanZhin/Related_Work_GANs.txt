Research
Data synthesis based on generative adversarial networks (Park et al. 2018)
* Input: original table, parameters to control privacy 
* Output: synthetic tables (categorical, discrete and continuous values) 
* Notes: Additional classifier neural network  ensure data reflects real work (e.g. must be male for prostate cancer)
General Adversarial Networks for Realistic Synthesis of Hyperspectral Samples (Audebert et al. 2018) 
* Input: image data (distribution of spectrum for each pixel in an image)
* Output: images (hyperspectral samples) 
* Notes: Images used to train deep networks (distinguish real vs fake)
Generative Adversarial Text to Image Synthesis (Reed et al. 2016) 
* Input: written descriptions of images 
* Output: image matching description 
* Notes: trains deep convolutional GAN 
Precomputed Real-Time Texture Synthesis with Markovian Generative Adversarial Networks (Li et al. 2016) 
* Input: photo 
* Output: stylised photo 
* Notes: decreases computational cost with a feed-forward, strided convolutional network 
Medical Image Synthesis with Context-Aware Generative Adversarial Networks (Nie et al. 2017) 
* Input: MRI
* Output: CT
* Notes: Auto-Context Model for contextual awareness 
Generative Adversarial Network-Based Postfilter for Statistical Parametric Speech Synthesis (Kaneko et al 2017) 
* Input: speech data set 
* Output: imitations of data set 
* Notes: GAN-based postfilter found to be effective 
 Generation of Synthetic Data with Generative Adversarial Networks
  DOI: 10.13140/RG.2.2.10557.92647 







http://www.vldb.org/pvldb/vol11/p1071-park.pdf 
https://hal.archives-ouvertes.fr/hal-01809872/document
http://proceedings.mlr.press/v48/reed16.pdf
https://link.springer.com/chapter/10.1007/978-3-319-46487-9_43
https://arxiv.org/abs/1612.05362 
http://www.kecl.ntt.co.jp/people/kameoka.hirokazu/publications/Kaneko2017ICASSP03_published.pdf 

