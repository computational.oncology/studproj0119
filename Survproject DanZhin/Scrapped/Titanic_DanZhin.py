# Load libraries
import psutil  ; print(list(psutil.virtual_memory())[0:2])
import numpy as np
import pandas as pd
import matplotlib.pyplot as plot
import sklearn.cluster
from sklearn.tree import DecisionTreeClassifier

#loading titanic data
train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")

#Drop features
train = train.drop(['Name','SibSp','Parch', 'Ticket', 'Fare', 'Cabin', 'Embarked'],axis=1)
test = test.drop(['Name','SibSp','Parch', 'Ticket', 'Fare', 'Cabin', 'Embarked'],axis=1)

# print(train.shape)
# print(train.columns)
train.head(3)

#Convert [Male,female] to binary [1,0]
for df in [train,test]:
    df["Sex_binary"]=df["Sex"].map({"male":1, "female":0})

#Fill in missing age values with 0, assuming no age = baby
train["Age"] = train["Age"].fillna(0)
test["Age"] = test["Age"].fillna(0)

#Features and target variable used to train
features = ['Pclass','Age','Sex_binary']
target = 'Survived'

train[features].head(3)

#Display first 3 target variables
train[target].head(3).values

#Creating and fitting decision DecisionTreeClassifier
from sklearn.tree import DecisionTreeClassifier

#Create classifier object with default hyperparameters
clf=DecisionTreeClassifier()

#Fit classifier using training features and training target values
clf.fit(train[features],train[target])

#Create decision tree ".dot" file
# from sklearn.tree import export_graphviz
# export_graphviz(clf,out_file='titanic_tree.dot',feature_names=features,rounded=True,filled=True,class_names=['Survived','Did not Survive'])

#Make predicitions using test data features
predictions=clf.predict(test[features])

#Display predicitions
predictions

#Create dataframe with passengers ids and predicitions
submission = pd.DataFrame({'PassengerId':test['PassengerId'],'Survived':predictions})
submission.head()

# csv
filename="Titanic_prediction_trial.csv"

submission.to_csv(filename,index=False)

print("saved file:" + filename)
#Means, mode and median
